# README #
## Description
Used during early system development.  Not used in prod.
Classes to parse the iex prop feeds from pcap files.


### data.propfeed.iex
Tools for working with IEX Prop Feed Data
- IEX TP Message Parser
- IEX TOPS Message Parser
- Message generator for TOPS messages (Uses templates in resources directory)

### io.pcap
Minimal pcap reader for tops files
- PcapNG - pcap file parser.  see example in main()
- TPPacketProcessor - wraps tops bytes to iterate messages
- PcapToTextConverter - command line util to convert the pcap files into text or csv (file per message type)

## Usage

### How to Build

```shell
MINGW64 /c/dev/git/proof-iex-parser (master)
$ ./gradlew -version

------------------------------------------------------------
Gradle 5.4.1
------------------------------------------------------------

Build time:   2019-04-26 08:14:42 UTC
Revision:     261d171646b36a6a28d5a19a69676cd098a4c19d

Kotlin:       1.3.21
Groovy:       2.5.4
Ant:          Apache Ant(TM) version 1.9.13 compiled on July 10 2018
JVM:          11.0.3 (AdoptOpenJDK 11.0.3+7)
OS:           Windows 10 10.0 amd64


MINGW64 /c/dev/git/proof-iex-parser (master)
$ ./gradlew clean build jar shadowJar
> Task :clean
> Task :compileJava
> Task :processResources
> Task :classes
> Task :jar
> Task :assemble
> Task :compileTestJava
> Task :processTestResources NO-SOURCE
> Task :testClasses
> Task :test
> Task :check
> Task :build
> Task :shadowJar

BUILD SUCCESSFUL in 6s
7 actionable tasks: 7 executed

MINGW64 /c/dev/git/proof-iex-parser (master)
$ ls -alhs build/libs/
total 24M
   0 drwxr-xr-x 1 btate 197609   0 Feb 16 07:56 .
4.0K drwxr-xr-x 1 btate 197609   0 Feb 16 07:56 ..
 24M -rw-r--r-- 1 btate 197609 24M Feb 16 07:56 proof-iex-parser-all.jar
 64K -rw-r--r-- 1 btate 197609 63K Feb 16 07:56 proof-iex-parser.jar
```

### PcapToTextConverter usage
```shell
USAGE: java com.prooftrading.core.io.pcap.PcapToTextConverter PCAP_FILE  OUTPUT_DIR  [CSV]
Convert the IEX TOPS binary file to text format, creating a file per message type

Example: java  -cp proof-iex-parser-all.jar com.prooftrading.core.io.pcap.PcapToTextConverter  C:/dev/data/pcapng/data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap  output

```

### Example 1: Convert Pcap to txt files
```shell
MINGW64 /c/dev/git/proof-iex-parser (master)
$ java -cp build/libs/proof-iex-parser-all.jar com.prooftrading.core.io.pcap.PcapToTextConverter C:/dev/data/pcapng/data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap  outputTxt
WARNING: sun.reflect.Reflection.getCallerClass is not supported. This will impact performance.
08:04:46.263 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - Creating output directory: C:\dev\git\proof-iex-parser\outputTxt
08:04:46.263 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - Converting Pcap File: inputFile=C:\dev\data\pcapng\data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap, outputDir=C:\dev\git\pr
oof-iex-parser\outputTxt, csv=false
08:04:46.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor - mapped
08:04:46.279 [main] INFO  com.prooftrading.core.io.pcap.PcapNG - mapFile(C:\dev\data\pcapng\data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap): range=[0-1073741824](1073.741824}, remaining=4843.398968
08:04:46.279 [main] INFO  com.prooftrading.core.io.pcap.PcapNG - ByteOrder=LITTLE_ENDIAN (byteMagic=4d3c2b1a)
08:04:47.435 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [ 1,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560174057475798, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=161209248} (863269 bps)
08:04:48.154 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [ 2,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560174667183570, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=321485016} (1366899 bps)
08:04:48.810 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [ 3,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560171025737430, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=481797484} (1513841 bps)

...

08:05:06.388 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [29,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560192194180971, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=371668044} (1362964 bps)
08:05:07.201 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [30,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560192492105110, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=532837584} (1248483 bps)
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor - Done - packets=[30,097,083], messages=[30,867,764], bytes=[2,556,025,162]   (1,433,194 pps | 1,469,894 mps)
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                  AuctionInfoMessage -      2,876
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                OfficialPriceMessage -          8
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -        OperationalHaltStatusMessage -      8,793
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                  QuoteUpdateMessage - 29,450,037
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -            SecurityDirectoryMessage -          4
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -     ShortSalePriceTestStatusMessage -      8,998
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                  SystemEventMessage -          6
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                  TradeReportMessage -  1,388,133
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                TradingStatusMessage -      8,909
08:05:07.279 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - Done - 30,097,085

MINGW64 /c/dev/git/proof-iex-parser (master)
$ find outputTxt/
outputTxt/
outputTxt/AuctionInfoMessage.txt
outputTxt/OfficialPriceMessage.txt
outputTxt/OperationalHaltStatusMessage.txt
outputTxt/QuoteUpdateMessage.txt
outputTxt/SecurityDirectoryMessage.txt
outputTxt/ShortSalePriceTestStatusMessage.txt
outputTxt/SystemEventMessage.txt
outputTxt/TradeReportMessage.txt
outputTxt/TradingStatusMessage.txt

MINGW64 /c/dev/git/proof-iex-parser (master)
$ head outputTxt/QuoteUpdateMessage.txt
QuoteUpdateMessage{flags=64, timestamp=1560165462218017797, symbol='A', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218020326, symbol='AA', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218023073, symbol='AAAU', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218025920, symbol='AABA', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218028641, symbol='AAC', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218031612, symbol='AADR', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218034215, symbol='AAL', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218036936, symbol='AAMC', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218040057, symbol='AAME', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
QuoteUpdateMessage{flags=64, timestamp=1560165462218042465, symbol='AAN', bidSize=0, bidPrice=0, askPrice=0, askSize=0}
```

### Example 1: Convert Pcap to csv files
```shell
MINGW64 /c/dev/git/proof-iex-parser (master)
$ java -cp build/libs/proof-iex-parser-all.jar com.prooftrading.core.io.pcap.PcapToTextConverter C:/dev/data/pcapng/data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap  outputCsv csv
WARNING: sun.reflect.Reflection.getCallerClass is not supported. This will impact performance.
08:07:30.357 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - Creating output directory: C:\dev\git\proof-iex-parser\outputCsv
08:07:30.373 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - Converting Pcap File: inputFile=C:\dev\data\pcapng\data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap, outputDir=C:\dev\git\pr
oof-iex-parser\outputCsv, csv=true
08:07:30.388 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor - mapped
08:07:30.388 [main] INFO  com.prooftrading.core.io.pcap.PcapNG - mapFile(C:\dev\data\pcapng\data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap): range=[0-1073741824](1073.741824}, remaining=4843.398968
08:07:30.388 [main] INFO  com.prooftrading.core.io.pcap.PcapNG - ByteOrder=LITTLE_ENDIAN (byteMagic=4d3c2b1a)
08:07:30.388 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.SystemEventMessage] fields=Timestamp,SystemEvent
08:07:30.388 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.ShortSalePriceTestStatusMessage] fields=Symbol,Timestamp,Detail,ShortSalePriceTestStatus

08:07:30.388 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.OperationalHaltStatusMessage] fields=Symbol,Timestamp,OperationalHaltStatus
08:07:30.388 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.TradingStatusMessage] fields=Reason,Symbol,Timestamp,TradingStatus
08:07:30.388 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.QuoteUpdateMessage] fields=Flags,Symbol,Timestamp,BidPrice,AskPrice,BidSize,AskSize
08:07:30.482 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.SecurityDirectoryMessage] fields=Flags,Symbol,Timestamp,RoundLotSize,LuldTier,AdjustedPO
CPrice
08:07:30.545 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.TradeReportMessage] fields=Size,Symbol,Timestamp,Price,TradeID,SaleConditionFlags
08:07:30.701 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.AuctionInfoMessage] fields=Symbol,Timestamp,AuctionType,PairedShares,ExtensionNumber,Imb
alanceSide,ReferencePrice,ImbalanceShares,IndicativeClearingPrice,AuctionBookClearingPrice,CollarReferencePrice,LowerAuctionCollar,UpperAuctionCollar,ScheduledAuctionTime
08:07:30.716 [main] INFO  com.prooftrading.core.io.pcap.CSVHelper - [com.prooftrading.core.data.propfeed.iex.tops.OfficialPriceMessage] fields=Symbol,Timestamp,PriceType,OfficialPrice
08:07:31.560 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [ 1,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560174057475798, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=161209248} (844264 bps)
08:07:32.295 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [ 2,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560174667183570, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=321485016} (1333313 bps)
08:07:32.967 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [ 3,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560171025737430, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=481797484} (1504595 bps)

...

08:07:50.811 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [28,000,000] EnhancedPacketBlock{length=204, interfaceID=0, timestamp=1560196040768988, capturedPacketLength=170, originalPa
cketLength=170, payloadStartOffset=210219700} (1339017 bps)
08:07:51.499 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [29,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560192194180971, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=371668044} (1439143 bps)
08:07:52.248 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - [30,000,000] EnhancedPacketBlock{length=160, interfaceID=0, timestamp=1560192492105110, capturedPacketLength=126, originalPa
cketLength=126, payloadStartOffset=532837584} (1341531 bps)
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor - Done - packets=[30,097,083], messages=[30,867,764], bytes=[2,556,025,162]   (1,371,916 pps | 1,407,045 mps)
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                  AuctionInfoMessage -      2,876
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                OfficialPriceMessage -          8
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -        OperationalHaltStatusMessage -      8,793
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                  QuoteUpdateMessage - 29,450,037
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -            SecurityDirectoryMessage -          4
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -     ShortSalePriceTestStatusMessage -      8,998
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                  SystemEventMessage -          6
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                  TradeReportMessage -  1,388,133
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.TPPacketProcessor -                TradingStatusMessage -      8,909
08:07:52.326 [main] INFO  com.prooftrading.core.io.pcap.PcapToTextConverter - Done - 30,097,085

MINGW64 /c/dev/git/proof-iex-parser (master)
$ find outputCsv/
outputCsv/
outputCsv/AuctionInfoMessage.csv
outputCsv/OfficialPriceMessage.csv
outputCsv/OperationalHaltStatusMessage.csv
outputCsv/QuoteUpdateMessage.csv
outputCsv/SecurityDirectoryMessage.csv
outputCsv/ShortSalePriceTestStatusMessage.csv
outputCsv/SystemEventMessage.csv
outputCsv/TradeReportMessage.csv
outputCsv/TradingStatusMessage.csv

MINGW64 /c/dev/git/proof-iex-parser (master)
$ head outputCsv/QuoteUpdateMessage.csv
Flags,Symbol,Timestamp,BidPrice,AskPrice,BidSize,AskSize
64,A,1560165462218017797,0,0,0,0
64,AA,1560165462218020326,0,0,0,0
64,AAAU,1560165462218023073,0,0,0,0
64,AABA,1560165462218025920,0,0,0,0
64,AAC,1560165462218028641,0,0,0,0
64,AADR,1560165462218031612,0,0,0,0
64,AAL,1560165462218034215,0,0,0,0
64,AAMC,1560165462218036936,0,0,0,0
64,AAME,1560165462218040057,0,0,0,0

```