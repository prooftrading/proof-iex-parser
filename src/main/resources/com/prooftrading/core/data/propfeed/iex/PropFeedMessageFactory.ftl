package ${package};

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

<#list imports as import>
import ${import};
</#list>

/**
* Generated Message Factory Class
*/
public class ${factoryClassName} implements PropFeedMessage.PropFeedMessageFactory {

    private static final Logger LOG = LoggerFactory.getLogger(${factoryClassName}.class);

    <#list messages as message>
    private final ${message.className} ${message.className?uncap_first} = new ${message.className}();
    </#list>

    @Override
    public PropFeedMessage fromTPMessage(TPPacket.TPMessage msg) {
        try {
            char msgType = PropFeedMessage.PropFeedMessageFactory.getMessageType(msg);
            switch (msgType) {
                <#list messages as message>
                case '${message.messageType}':   return  ${message.className?uncap_first}.init(msg);
                </#list>
                default:
                    LOG.warn("Unknown msgType={}, msg={}", msgType, msg);
                    break;
            }
        } catch (Exception ex) {
            LOG.error("Error parsing msg: " + msg, ex);
        }
        return null;
    }

}