package ${package};

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;
<#list imports as import>
import ${import};
</#list>

/**
 * Generated Message Class
 */
public class ${className} extends ${baseClassName!"PropFeedMessage.AbstractPropFeedMessage"} {

    public static final char MESSAGE_TYPE = '${messageType}';

    <#list fields as field>
        <#if (field.constants)??>
            <#list field.constants as constant>
    public static final <#if (constant.type)??>${constant.type}<#else>${field.type}</#if> ${field.name?upper_case}_${constant.name?upper_case} = ${constant.value};
            </#list>

        </#if>
    </#list>
    <#list fields as field>
        <#if field.type == "String" >
    private StringBuilder ${field.name} = new StringBuilder(${field.default});
        <#else>
    private ${field.type} ${field.name} = ${field.default};
        </#if>
    </#list>

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        <#list fields as field>
        <#if field.type == "String" >
        this.${field.name} = msg.getString(${field.offset}, ${field.length}, this.${field.name});
        <#else>
        this.${field.name} = msg.get${field.type?cap_first}(${field.offset});
        </#if>
        </#list>
        return this;
    }

    <#list fields as field>
    public <#if field.type == "String" >StringBuilder<#else>${field.type}</#if> get${field.name?cap_first}() {
        return this.${field.name};
    }

    </#list>
    @Override
    public String toString() {
        return "${className}{" +
            <#list fields as field>
                <#if field.type == "String">
                "<#if field?index !=0 >, </#if>${field.name}='" + ${field.name} + "'" +
                <#else>
                "<#if field?index !=0 >, </#if>${field.name}=" + ${field.name} +
                </#if>
            </#list>
                '}';
    }
}