package com.prooftrading.core.io.pcap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class to create csv data from objects.
 * Uses reflection to extract data.
 */
public class CSVHelper {
    private static final Logger LOG = LoggerFactory.getLogger(CSVHelper.class);

    private final List<Method> methods = new ArrayList<>();
    private final String header;
    private final StringBuilder sbRecordBuffer = new StringBuilder();

    public CSVHelper(Class<?> clazz) {

        StringBuilder sbHeader = new StringBuilder();
        for(Method method : clazz.getDeclaredMethods()) {
            if (method.getParameterCount() == 0 && method.getName().startsWith("get")) {
                methods.add(method);

                if (sbHeader.length() > 0) {
                    sbHeader.append(",");
                }
                String fieldName = method.getName().substring(3);
                sbHeader.append(fieldName);
            }
        }

        this.header = sbHeader.toString();
        LOG.info("[{}] fields={}", clazz.getName(), header);
    }

    public String getHeader(){
        return this.header;
    }

    @SuppressWarnings("ForLoopReplaceableByForEach")
    public String toRecord(Object obj) {
        this.sbRecordBuffer.setLength(0);

        for(int i = 0, n = methods.size(); i < n; i++) {
            Object val = null;
            try {
                val = methods.get(i).invoke(obj);
            } catch (IllegalAccessException|InvocationTargetException e) {
                LOG.error("Error extracting field value: object=" + obj, e);
            }
            if (sbRecordBuffer.length() > 0) {
                sbRecordBuffer.append(",");
            }
            sbRecordBuffer.append(val);
        }

        return sbRecordBuffer.toString();
    }
}
