package com.prooftrading.core.io.pcap;

public class RateSensor {
    private final int interval;
    private long nextInterval;
    private long lastInterval;

    private long count;
    private long lastLogTimeNs = System.nanoTime();
    private double rate;

    public RateSensor(int interval) {
        this.interval = interval;
        this.nextInterval = interval;
    }

    public boolean addObservations(int n) {

        this.count += n;
        if (this.count >= this.nextInterval) {
            long now = System.nanoTime();
            double duration = (now - this.lastLogTimeNs) / 1_000_000_000d;
            this.rate = (this.nextInterval - this.lastInterval) / duration;
            this.lastLogTimeNs = now;
            this.lastInterval = this.nextInterval;
            this.nextInterval += interval;
            return true;
        }

        return false;
    }

    public int getInterval() {
        return interval;
    }

    public long getCount() {
        return count;
    }

    public double getRate() {
        return rate;
    }
}
