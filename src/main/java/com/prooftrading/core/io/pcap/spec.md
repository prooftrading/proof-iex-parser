<table class="header">

<tbody>

<tr>

<td class="left">Network Working Group</td>

<td class="right">M. Tuexen, Ed.</td>

</tr>

<tr>

<td class="left">Internet-Draft</td>

<td class="right">Muenster Univ. of Appl. Sciences</td>

</tr>

<tr>

<td class="left">Intended status: Informational</td>

<td class="right">F. Risso</td>

</tr>

<tr>

<td class="left">Expires: March 6, 2016</td>

<td class="right">Politecnico di Torino</td>

</tr>

<tr>

<td class="left"></td>

<td class="right">J. Bongertz</td>

</tr>

<tr>

<td class="left"></td>

<td class="right">Airbus DS CyberSecurity</td>

</tr>

<tr>

<td class="left"></td>

<td class="right">G. Combs</td>

</tr>

<tr>

<td class="left"></td>

<td class="right">Wireshark</td>

</tr>

<tr>

<td class="left"></td>

<td class="right">G. Harris</td>

</tr>

<tr>

<td class="left"></td>

<td class="right">September 3, 2015</td>

</tr>

</tbody>

</table>

PCAP Next Generation (pcapng) Capture File Format  
<span class="filename">draft-tuexen-opsawg-pcapng</span>

# [Abstract](#rfc.abstract)

This document describes a format to record captured packets to a file. This format is extensible; Wireshark can currently read and write it, and libpcap can currently read some pcapng files.

# [Status of This Memo](#rfc.status)

This Internet-Draft is submitted in full conformance with the provisions of BCP 78 and BCP 79.

Internet-Drafts are working documents of the Internet Engineering Task Force (IETF). Note that other groups may also distribute working documents as Internet-Drafts. The list of current Internet-Drafts is at http://datatracker.ietf.org/drafts/current/.

Internet-Drafts are draft documents valid for a maximum of six months and may be updated, replaced, or obsoleted by other documents at any time. It is inappropriate to use Internet-Drafts as reference material or to cite them other than as "work in progress."

This Internet-Draft will expire on March 6, 2016.

# [Copyright Notice](#rfc.copyrightnotice)

Copyright (c) 2015 IETF Trust and the persons identified as the document authors. All rights reserved.

This document is subject to BCP 78 and the IETF Trust's Legal Provisions Relating to IETF Documents (http://trustee.ietf.org/license-info) in effect on the date of publication of this document. Please review these documents carefully, as they describe your rights and restrictions with respect to this document. Code Components extracted from this document must include Simplified BSD License text as described in Section 4.e of the Trust Legal Provisions and are provided without warranty as described in the Simplified BSD License.

* * *

# [Table of Contents](#rfc.toc)

*   1\. [Introduction](#rfc.section.1)
*   2\. [Terminology](#rfc.section.2)

*   2.1\. [Acronyms](#rfc.section.2.1)

*   3\. [General File Structure](#rfc.section.3)

*   3.1\. [General Block Structure](#rfc.section.3.1)
*   3.2\. [Block Types](#rfc.section.3.2)
*   3.3\. [Logical Block Hierarchy](#rfc.section.3.3)
*   3.4\. [Physical File Layout](#rfc.section.3.4)
*   3.5\. [Options](#rfc.section.3.5)

*   3.5.1\. [Custom Options](#rfc.section.3.5.1)

*   3.6\. [Data format](#rfc.section.3.6)

*   3.6.1\. [Endianness](#rfc.section.3.6.1)
*   3.6.2\. [Alignment](#rfc.section.3.6.2)

*   4\. [Block Definition](#rfc.section.4)

*   4.1\. [Section Header Block](#rfc.section.4.1)
*   4.2\. [Interface Description Block](#rfc.section.4.2)
*   4.3\. [Enhanced Packet Block](#rfc.section.4.3)

*   4.3.1\. [Enhanced Packet Block Flags Word](#rfc.section.4.3.1)

*   4.4\. [Simple Packet Block](#rfc.section.4.4)
*   4.5\. [Name Resolution Block](#rfc.section.4.5)
*   4.6\. [Interface Statistics Block](#rfc.section.4.6)
*   4.7\. [Custom Block](#rfc.section.4.7)

*   5\. [Experimental Blocks (deserve further investigation)](#rfc.section.5)

*   5.1\. [Alternative Packet Blocks (experimental)](#rfc.section.5.1)
*   5.2\. [Compression Block (experimental)](#rfc.section.5.2)
*   5.3\. [Encryption Block (experimental)](#rfc.section.5.3)
*   5.4\. [Fixed Length Block (experimental)](#rfc.section.5.4)
*   5.5\. [Directory Block (experimental)](#rfc.section.5.5)
*   5.6\. [Traffic Statistics and Monitoring Blocks (experimental)](#rfc.section.5.6)
*   5.7\. [Event/Security Block (experimental)](#rfc.section.5.7)

*   6\. [Vendor-Specific Custom Extensions](#rfc.section.6)

*   6.1\. [Supported Use-Cases](#rfc.section.6.1)
*   6.2\. [Controlling Copy Behavior](#rfc.section.6.2)
*   6.3\. [Strings vs. Bytes](#rfc.section.6.3)
*   6.4\. [Endianness Issues](#rfc.section.6.4)

*   7\. [Recommended File Name Extension: .pcapng](#rfc.section.7)
*   8\. [Conclusions](#rfc.section.8)
*   9\. [Implementations](#rfc.section.9)
*   10\. [Security Considerations](#rfc.section.10)
*   11\. [IANA Considerations](#rfc.section.11)

*   11.1\. [Standardized Block Type Codes](#rfc.section.11.1)

*   12\. [Contributors](#rfc.section.12)
*   13\. [Acknowledgments](#rfc.section.13)
*   14\. [Normative References](#rfc.references)
*   Appendix A. [Packet Block (obsolete!)](#rfc.appendix.A)
*   [Authors' Addresses](#rfc.authors)

# [1.](#rfc.section.1) Introduction

The problem of exchanging packet traces becomes more and more critical every day; unfortunately, no standard solutions exist for this task right now. One of the most accepted packet interchange formats is the one defined by libpcap, which is rather old and is lacking in functionality for more modern applications particularly from the extensibility point of view.

This document proposes a new format for recording packet traces. The following goals are being pursued:

<dl>

<dt>Extensibility:</dt>

<dd style="margin-left: 8">It should be possible to add new standard capabilities to the file format over time, and third parties should be able to enrich the information embedded in the file with proprietary extensions, with tools unaware of newer extensions being able to ignore them.</dd>

<dt>Portability:</dt>

<dd style="margin-left: 8">A capture trace must contain all the information needed to read data independently from network, hardware and operating system of the machine that made the capture.</dd>

<dt>Merge/Append data:</dt>

<dd style="margin-left: 8">It should be possible to add data at the end of a given file, and the resulting file must still be readable.</dd>

</dl>

# [2.](#rfc.section.2) Terminology

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [[RFC2119]](#RFC2119).

# [2.1.](#rfc.section.2.1) Acronyms

The following acronyms are used throughout this document:

<dl>

<dt>SHB:</dt>

<dd style="margin-left: 8">Section Header Block</dd>

<dt>IDB:</dt>

<dd style="margin-left: 8">Interface Description Block</dd>

<dt>ISB:</dt>

<dd style="margin-left: 8">Interface Statistics Block</dd>

<dt>EPB:</dt>

<dd style="margin-left: 8">Enhanced Packet Block</dd>

<dt>SPB:</dt>

<dd style="margin-left: 8">Simple Packet Block</dd>

<dt>NRB:</dt>

<dd style="margin-left: 8">Name Resolution Block</dd>

<dt>CB:</dt>

<dd style="margin-left: 8">Custom Block</dd>

</dl>

# [3.](#rfc.section.3) General File Structure

# [3.1.](#rfc.section.3.1) [General Block Structure](#section_block)

A capture file is organized in blocks, that are appended one to another to form the file. All the blocks share a common format, which is shown in [Figure 1](#formatblock).

<div id="rfc.figure.1">

<div id="formatblock">

<pre> 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                          Block Type                           |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                      Block Total Length                       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/                          Block Body                           /
/              variable length, padded to 32 bits               /
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                      Block Total Length                       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</pre>

Figure 1: Basic block structure.

The fields have the following meaning:

*   Block Type (32 bits): unique value that identifies the block. Values whose Most Significant Bit (MSB) is equal to 1 are reserved for local use. They can be used to make extensions to the file format to save private data to the file. The list of currently defined types can be found in [Section 11.1](#section_block_code_registry).
*   Block Total Length: total size of this block, in octets. For instance, the length of a block that does not have a body is 12 octets: 4 octets for the Block Type, 4 octets for the initial Block Total Length and 4 octets for the trailing Block Total Length. This value MUST be a multiple of 4.
*   Block Body: content of the block.
*   Block Total Length: total size of this block, in octets. This field is duplicated to permit backward file navigation.

This structure, shared among all blocks, makes it easy to process a file and to skip unneeded or unknown blocks. Some blocks can contain other blocks inside (nested blocks). Some of the blocks are mandatory, i.e. a capture file is not valid if they are not present, other are optional.

The General Block Structure allows defining other blocks if needed. A parser that does not understand them can simply ignore their content.

# [3.2.](#rfc.section.3.2) Block Types

The currently standardized Block Type codes are specified in [Section 11.1](#section_block_code_registry); they have been grouped in the following four categories:

The following MANDATORY block MUST appear at least once in each file:

*   [Section Header Block](#section_shb) <cite title="NONE">[section_shb]</cite>: it defines the most important characteristics of the capture file.

The following OPTIONAL blocks MAY appear in a file:

*   [Interface Description Block](#section_idb) <cite title="NONE">[section_idb]</cite>: it defines the most important characteristics of the interface(s) used for capturing traffic. This block is required in certain cases, as described later.
*   [Enhanced Packet Block](#section_epb) <cite title="NONE">[section_epb]</cite>: it contains a single captured packet, or a portion of it. It represents an evolution of the original, now obsolete, [Packet Block](#appendix_pb) <cite title="NONE">[appendix_pb]</cite>. If this appears in a file, an Interface Description Block is also required, before this block.
*   [Simple Packet Block](#section_spb) <cite title="NONE">[section_spb]</cite>: it contains a single captured packet, or a portion of it, with only a minimal set of information about it. If this appears in a file, an Interface Description Block is also required, before this block.
*   [Name Resolution Block](#section_nrb) <cite title="NONE">[section_nrb]</cite>: it defines the mapping from numeric addresses present in the packet capture and the canonical name counterpart.
*   [Interface Statistics Block](#section_isb) <cite title="NONE">[section_isb]</cite>: it defines how to store some statistical data (e.g. packet dropped, etc) which can be useful to understand the conditions in which the capture has been made. If this appears in a file, an Interface Description Block is also required, before this block.
*   [Custom Block](#section_custom_block) <cite title="NONE">[section_custom_block]</cite>: it contains vendor-specific data in a portable fashion.

The following OBSOLETE block SHOULD NOT appear in newly written files (but is documented in the Appendix for reference):

*   [Packet Block](#appendix_pb) <cite title="NONE">[appendix_pb]</cite>: it contains a single captured packet, or a portion of it. It is OBSOLETE, and superseded by the [Enhanced Packet Block](#section_epb) <cite title="NONE">[section_epb]</cite>.

The following EXPERIMENTAL blocks are considered interesting but the authors believe that they deserve more in-depth discussion before being defined:

*   Alternative Packet Blocks
*   Compression Block
*   Encryption Block
*   Fixed Length Block
*   Directory Block
*   Traffic Statistics and Monitoring Blocks
*   Event/Security Blocks

# [3.3.](#rfc.section.3.3) Logical Block Hierarchy

The blocks build a logical hierarchy as they refer to each other. [Figure 2](#block-hierarchy) shows the logical hierarchy of the currently defined blocks in the form of a "tree view":

<div id="rfc.figure.2">

<div id="block-hierarchy">

<pre>Section Header
|
+- Interface Description
|  +- Simple Packet
|  +- Enhanced Packet
|  +- Interface Statistics
|
+- Name Resolution
</pre>

Figure 2: Logical Block Hierarchy of a pcapng File

For example: each captured packet refers to a specific capture interface, the interface itself refers to a specific section.

# [3.4.](#rfc.section.3.4) Physical File Layout

The file MUST begin with a Section Header Block. However, more than one Section Header Block can be present in the capture file, each one covering the data following it until the next one (or the end of file). A Section includes the data delimited by two Section Header Blocks (or by a Section Header Block and the end of the file), including the first Section Header Block.

In case an application cannot read a Section because of different version number, it MUST skip everything until the next Section Header Block. Note that, in order to properly skip the blocks until the next section, all blocks MUST have the fields Type and Length at the beginning. In order to preperly skip blocks in the backward direction, all blocks MUST have the Length repeated at the end of the block. These are mandatory requirements that MUST be maintained in future versions of the block format.

[Figure 3](#fssample-SHB) shows a typical file layout, with a single Section Header that covers the whole file.

<div id="rfc.figure.3">

<div id="fssample-SHB">

<pre>+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| SHB v1.0  |                      Data                         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</pre>

Figure 3: File structure example: Typical layout with a single Section Header Block

[Figure 4](#fssample-SHB3) shows a file that contains three headers, and is normally the result of file concatenation. An application that understands only version 1.0 of the file format skips the intermediate section and restart processing the packets after the third Section Header.

<div id="rfc.figure.4">

<div id="fssample-SHB3">

<pre>|--   1st Section   --|--   2nd Section   --|--  3rd Section  --|
|                                                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| SHB v1.0  |  Data   | SHB V1.1  |  Data   | SHB V1.0  |  Data |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</pre>

Figure 4: File structure example: three Section Header Blocks in a single file

[Figure 5](#fssample-minimum) shows a file comparable to a "classic libpcap" file - the minimum for a useful capture file. It contains a single Section Header Block (SHB), a single Interface Description Block (IDB) and a few Enhanced Packet Blocks (EPB).

<div id="rfc.figure.5">

<div id="fssample-minimum">

<pre>+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| SHB | IDB | EPB | EPB |    ...    | EPB |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</pre>

Figure 5: File structure example: a pcapng file similar to a classical libpcap file

[Figure 6](#fssample-full) shows a complex example file. In addition to the minimum file above, it contains packets captured from three interfaces, capturing on the third of which begins after packets have arrived on other interfaces, and also includes some Name Resolution Blocks (NRB) and an Interface Statistics Block (ISB).

<div id="rfc.figure.6">

<div id="fssample-full">

<pre>+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| SHB | IDB | IDB | EPB | NRB |...| IDB | EPB | ISB | NRB | EPB |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
</pre>

Figure 6: File structure example: complex pcapng file

The last example should make it obvious that the block structure makes the file format very flexible compared to the classical libpcap format.

# [3.5.](#rfc.section.3.5) [Options](#section_opt)

All the block bodies have the possibility to embed optional fields. Optional fields can be used to insert some information that may be useful when reading data, but that is not really needed for packet processing. Therefore, each tool can either read the content of the optional fields (if any), or skip some of them or even all at once.

Skipping all the optional fields at once is straightforward because most of the blocks are made of a first part with fixed format, and a second optional part. Therefore, the Block Length field (present in the General Block Structure, see [Section 3.1](#section_block)) can be used to skip everything till the next block.

Options are a list of Type - Length - Value fields, each one containing a single value:

*   Option Type (2 octets): it contains the code that specifies the type of the current TLV record. Option types whose Most Significant Bit is equal to one are reserved for local use; therefore, there is no guarantee that the code used is unique among all capture files (generated by other applications), and is most certainly not portable. For cross-platform gloablly unique vendor-specific extensions, the Custom Option MUST be used instead, as defined in [Section 3.5.1](#section_custom_option)).
*   Option Length (2 octets): it contains the actual length of the following 'Option Value' field without the padding octets.
*   Option Value (variable length): it contains the value of the given option, padded to a 32-bit boundary. The actual length of this field (i.e. without the padding octets) is specified by the Option Length field.

If an option's value is a string, the value is not necessarily zero-terminated. Software that reads these files MUST NOT assume that strings are zero-terminated, and MUST treat a zero-value octet as a string terminator.

Options may be repeated several times (e.g. an interface that has several IP addresses associated to it) TODO: mention for each option, if it can/shouldn't appear more than one time. The option list is terminated by a Option which uses the special 'End of Option' code (opt_endofopt).

The format of the optional fields is shown in [Figure 7](#formatopt).

<div id="rfc.figure.7">

<div id="formatopt">

<pre> 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|      Option Code              |         Option Length         |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/                       Option Value                            /
/              variable length, padded to 32 bits               /
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/                                                               /
/                 . . . other options . . .                     /
/                                                               /
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   Option Code == opt_endofopt  |  Option Length == 0          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 </pre>

Figure 7: Options Format

The following codes can always be present in any optional field:

<div id="rfc.table.1">

<div id="optionsall">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Common Options</caption>

<thead>

<tr>

<th class="left">Name</th>

<th class="left">Code</th>

<th class="left">Length</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">opt_endofopt</td>

<td class="left">0</td>

<td class="left">0</td>

</tr>

<tr>

<td class="left">opt_comment</td>

<td class="left">1</td>

<td class="left">variable</td>

</tr>

<tr>

<td class="left">opt_custom</td>

<td class="left">2988/2989/19372/19373</td>

<td class="left">variable</td>

</tr>

</tbody>

</table>

<dl>

<dt>opt_endofopt:</dt>

<dd style="margin-left: 8">  
The opt_endofopt option delimits the end of the optional fields. This option MUST NOT be repeated within a given list of options.</dd>

<dt>opt_comment:</dt>

<dd style="margin-left: 8">  
The opt_comment option is a UTF-8 string containing human-readable comment text that is associated to the current block. Line separators SHOULD be a carriage-return + linefeed ('\r\n') or just linefeed ('\n'); either form may appear and be considered a line separator. The string is not null-terminated.</dd>

<dd style="margin-left: 8">Examples: "This packet is the beginning of all of our problems", "Packets 17-23 showing a bogus TCP retransmission!\r\n This is reported in bugzilla entry 1486.\nIt will be fixed in the future.".</dd>

<dt>opt_custom:</dt>

<dd style="margin-left: 8">  
This option is described in detail in [Section 3.5.1](#section_custom_option).</dd>

</dl>

# [3.5.1.](#rfc.section.3.5.1) [Custom Options](#section_custom_option)

Customs Options are used for portable, vendor-specific data related to the block they're in. A Custom Option can be in any block type that can have options, can be repeated any number of times in a block, and may come before or after other option types - except the opt_endofopt which is always the last option. Different Custom Options, of different type codes and/or different Private Enterprise Numbers, may be used in the same pcapng file. See [Section 6](#section_vendor) for additional details.

<div id="rfc.figure.8">

<div id="formatcustomopt">

<pre> 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     Custom Option Code        |         Option Length         |
+---------------------------------------------------------------+
|                Private Enterprise Number (PEN)                |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/                        Custom Data                            /
/              variable length, padded to 32 bits               /
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 </pre>

Figure 8: Custom Options Format

The Custom Option has the following fields:

*   Custom Option Code: The code number for the Custom Option, which can be one of the following decimal numbers:

    <dl>

    <dt>2988:</dt>

    <dd style="margin-left: 8">  
    This option code identifies a Custom Option containing a UTF-8 string in the Custom Data portion, without NULL termination. This Custom Option can be safely copied to a new file if the pcapng file is manipulated by an application; otherwise 19372 should be used instead. See [Section 6.2](#section_vendor_copy) for details.</dd>

    <dt>2989:</dt>

    <dd style="margin-left: 8">  
    This option code identifies a Custom Option containing binary octets in the Custom Data portion. This Custom Option can be safely copied to a new file if the pcapng file is manipulated by an application; otherwise 19372 should be used instead. See [Section 6.2](#section_vendor_copy) for details.</dd>

    <dt>19372:</dt>

    <dd style="margin-left: 8">  
    This option code identifies a Custom Option containing a UTF-8 string in the Custom Data portion, without NULL termination. This Custom Option should not be copied to a new file if the pcapng file is manipulated by an application. See [Section 6.2](#section_vendor_copy) for details.</dd>

    <dt>19373:</dt>

    <dd style="margin-left: 8">  
    This option code identifies a Custom Option containing binary octets in the Custom Data portion. This Custom Option should not be copied to a new file if the pcapng file is manipulated by an application. See [Section 6.2](#section_vendor_copy) for details.</dd>

    </dl>

*   Option Length: as described in [Section 3.1](#section_block), this contains the length of the option's value, which includes the 4-octet Private Enterprise Number and variable-length Custom Data fields, without the padding octets.
*   Private Enterprise Number: An IANA-assigned Private Enterprise Number identifying the organization which defined the Custom Option. See [Section 6.1](#section_vendor_uses) for details. The PEN number MUST be encoded using the same endianness as the Section Header Block it is within the scope of.
*   Custom Data: the custom data, padded to a 32 bit boundary.

# [3.6.](#rfc.section.3.6) Data format

# [3.6.1.](#rfc.section.3.6.1) Endianness

Data contained in each section will always be saved according to the characteristics (little endian / big endian) of the capturing machine. This refers to all the fields that are saved as numbers and that span over two or more octets.

The approach of having each section saved in the native format of the generating host is more efficient because it avoids translation of data when reading / writing on the host itself, which is the most common case when generating/processing capture captures.

Please note: The endianness is indicated by the [Section Header Block](#section_shb) <cite title="NONE">[section_shb]</cite>. Since this block can appear several times in a pcapng file, a single file can contain both endianness variants.

# [3.6.2.](#rfc.section.3.6.2) Alignment

All fields of this specification uses proper alignment for 16- and 32-bit values. This makes it easier and faster to read/write file contents if using techniques like memory mapped files.

The alignment octets (marked in this document e.g. with "padded to 32 bits") MUST be filled with zeroes.

Please note: 64-bit values are not aligned to 64-bit boundaries. This is because the file is naturally aligned to 32-bit boundaries only. Special care MUST be taken when reading and writing such values. TODO: the spec is not too consistent wrt how 64-bit values are saved. In the Packet blocks we clearly specify where the low and high 32-bits of a 64-bit timestamp should be saved. In the SHB we do use the endianness of the machine when we save the section length.

# [4.](#rfc.section.4) Block Definition

This section details the format of the blocks currently defined.

# [4.1.](#rfc.section.4.1) [Section Header Block](#section_shb)

The Section Header Block (SHB) is mandatory. It identifies the beginning of a section of the capture capture file. The Section Header Block does not contain data but it rather identifies a list of blocks (interfaces, packets) that are logically correlated. Its format is shown in [Figure 9](#format_shb).

<div id="rfc.figure.9">

<div id="format_shb">

<pre>   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------------------------------------------------------+
 0 |                   Block Type = 0x0A0D0D0A                     |
   +---------------------------------------------------------------+
 4 |                      Block Total Length                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 8 |                      Byte-Order Magic                         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
12 |          Major Version        |         Minor Version         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
16 |                                                               |
   |                          Section Length                       |
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
24 /                                                               /
   /                      Options (variable)                       /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Block Total Length                       |
   +---------------------------------------------------------------+
</pre>

Figure 9: Section Header Block Format

The meaning of the fields is:

*   Block Type: The block type of the Section Header Block is the integer corresponding to the 4-char string "\r\n\n\r" (0x0A0D0D0A). This particular value is used for 2 reasons:
    1.  This number is used to detect if a file has been transferred via FTP or HTTP from a machine to another with an inappropriate ASCII conversion. In this case, the value of this field will differ from the standard one ("\r\n\n\r") and the reader can detect a possibly corrupted file.
    2.  This value is palindromic, so that the reader is able to recognize the Section Header Block regardless of the endianness of the section. The endianness is recognized by reading the Byte Order Magic, that is located 8 octets after the Block Type.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Byte-Order Magic: magic number, whose value is the hexadecimal number 0x1A2B3C4D. This number can be used to distinguish sections that have been saved on little-endian machines from the ones saved on big-endian machines.
*   Major Version: number of the current mayor version of the format. Current value is 1\. This value should change if the format changes in such a way that tools that can read the new format could not read the old format (i.e., the code would have to check the version number to be able to read both formats).
*   Minor Version: number of the current minor version of the format. Current value is 0\. This value should change if the format changes in such a way that tools that can read the new format can still automatically read the new format but code that can only read the old format cannot read the new format.
*   Section Length: a signed 64-bit value specifying the length in octets of the following section, excluding the Section Header Block itself. This field can be used to skip the section, for faster navigation inside large files. Section Length equal -1 (0xFFFFFFFFFFFFFFFF) means that the size of the section is not specified, and the only way to skip the section is to parse the blocks that it contains. Please note that if this field is valid (i.e. not negative), its value is always aligned to 32 bits, as all the blocks are aligned to and padded to 32-bit boundaries. Also, special care should be taken in accessing this field: since the alignment of all the blocks in the file is 32-bits, this field is not guaranteed to be aligned to a 64-bit boundary. This could be a problem on 64-bit processors.
*   Options: optionally, a list of options (formatted according to the rules defined in [Section 3.5](#section_opt)) can be present.

Adding new block types or options would not necessarily require that either Major or Minor numbers be changed, as code that does not know about the block type or option should just skip it; only if skipping a block or option does not work should the minor version number be changed.

Aside from the options defined in [Section 3.5](#section_opt), the following options are valid within this block:

<div id="rfc.table.2">

<div id="options_shb">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Section Header Block Options</caption>

<thead>

<tr>

<th class="left">Name</th>

<th class="left">Code</th>

<th class="left">Length</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">shb_hardware</td>

<td class="left">2</td>

<td class="left">variable</td>

</tr>

<tr>

<td class="left">shb_os</td>

<td class="left">3</td>

<td class="left">variable</td>

</tr>

<tr>

<td class="left">shb_userappl</td>

<td class="left">4</td>

<td class="left">variable</td>

</tr>

</tbody>

</table>

<dl>

<dt>shb_hardware:</dt>

<dd style="margin-left: 8">  
The shb_hardware option is a UTF-8 string containing the description of the hardware used to create this section.</dd>

<dd style="margin-left: 8">Examples: "x86 Personal Computer", "Sun Sparc Workstation".</dd>

<dt>shb_os:</dt>

<dd style="margin-left: 8">  
The shb_os option is a UTF-8 string containing the name of the operating system used to create this section.</dd>

<dd style="margin-left: 8">Examples: "Windows XP SP2", "openSUSE 10.2".</dd>

<dt>shb_userappl:</dt>

<dd style="margin-left: 8">  
The shb_userappl option is a UTF-8 string containing the name of the application used to create this section.</dd>

<dd style="margin-left: 8">Examples: "dumpcap V0.99.7".</dd>

</dl>

[Open issue: does a program which re-writes a capture file change the original hardware/os/application info?]

# [4.2.](#rfc.section.4.2) [Interface Description Block](#section_idb)

An Interface Description Block (IDB) is the container for information describing an interface on which packet data is captured.

Tools that write / read the capture file associate a progressive 32-bit number (starting from '0') to each Interface Definition Block, called the Interface ID for the interface in question. This number is unique within each Section and identifies the interface to which the IDB refers; it is only unique inside the current section, so, two Sections can have different interfaces identified by the same Interface ID values. This unique identifier is referenced by other blocks, such as Enhanced Packet Blocks and Interface Statistic Blocks, to indicate the interface to which the block refers (such the interface that was used to capture the packet that an Enhanced Packet Block contains or to which the statistics in an Interface Statistic Block refer).

There must be an Interface Description Block for each interface to which another block refers. Blocks such as an Enhanced Packet Block or an Interface Statistics Block contain an Interface ID value referring to a particular interface, and a Simple Packet Block implicitly refers to an interface with an Interface ID of 0\. If the file does not contain any blocks that use an Interface ID, then the file does not need to have any IDBs.

An Interface Description Block is valid only inside the section which it belongs to. The structure of a Interface Description Block is shown in [Figure 10](#format_idb).

<div id="rfc.figure.10">

<div id="format_idb">

<pre>    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------------------------------------------------------+
 0 |                    Block Type = 0x00000001                    |
   +---------------------------------------------------------------+
 4 |                      Block Total Length                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 8 |           LinkType            |           Reserved            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
12 |                            SnapLen                            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
16 /                                                               /
   /                      Options (variable)                       /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Block Total Length                       |
   +---------------------------------------------------------------+
 </pre>

Figure 10: Interface Description Block Format

The meaning of the fields is:

*   Block Type: The block type of the Interface Description Block is 1.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   LinkType: a value that defines the link layer type of this interface. The list of Standardized Link Layer Type codes is available in [the tcpdump.org link-layer header types registry](http://www.tcpdump.org/linktypes.html).
*   Reserved: not used - MUST be filled with 0, and ignored by pcapng file readers.
*   SnapLen: maximum number of octets captured from each packet. The portion of each packet that exceeds this value will not be stored in the file. A value of zero indicates no limit.
*   Options: optionally, a list of options (formatted according to the rules defined in [Section 3.5](#section_opt)) can be present.

In addition to the options defined in [Section 3.5](#section_opt), the following options are valid within this block:

<div id="rfc.table.3">

<div id="optionsifb">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Interface Description Block Options</caption>

<thead>

<tr>

<th class="left">Name</th>

<th class="left">Code</th>

<th class="left">Length</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">if_name</td>

<td class="left">2</td>

<td class="left">Variable</td>

</tr>

<tr>

<td class="left">if_description</td>

<td class="left">3</td>

<td class="left">Variable</td>

</tr>

<tr>

<td class="left">if_IPv4addr</td>

<td class="left">4</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">if_IPv6addr</td>

<td class="left">5</td>

<td class="left">17</td>

</tr>

<tr>

<td class="left">if_MACaddr</td>

<td class="left">6</td>

<td class="left">6</td>

</tr>

<tr>

<td class="left">if_EUIaddr</td>

<td class="left">7</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">if_speed</td>

<td class="left">8</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">if_tsresol</td>

<td class="left">9</td>

<td class="left">1</td>

</tr>

<tr>

<td class="left">if_tzone</td>

<td class="left">10</td>

<td class="left">4</td>

</tr>

<tr>

<td class="left">if_filter</td>

<td class="left">11</td>

<td class="left">variable</td>

</tr>

<tr>

<td class="left">if_os</td>

<td class="left">12</td>

<td class="left">variable</td>

</tr>

<tr>

<td class="left">if_fcslen</td>

<td class="left">13</td>

<td class="left">1</td>

</tr>

<tr>

<td class="left">if_tsoffset</td>

<td class="left">14</td>

<td class="left">8</td>

</tr>

</tbody>

</table>

<dl>

<dt>if_name:</dt>

<dd style="margin-left: 8">  
The if_name option is a UTF-8 string containing the name of the device used to capture data.</dd>

<dd style="margin-left: 8">Examples: "eth0", "\Device\NPF_{AD1CE675-96D0-47C5-ADD0-2504B9126B68}".</dd>

<dt>if_description:</dt>

<dd style="margin-left: 8">  
The if_description option is a UTF-8 string containing the description of the device used to capture data.</dd>

<dd style="margin-left: 8">Examples: "Broadcom NetXtreme", "First Ethernet Interface".</dd>

<dt>if_IPv4addr:</dt>

<dd style="margin-left: 8">  
The if_IPv4addr option is an IPv4 network address and corresponding netmask for the interface. The first four octets are the IP address, and the next four octets are the netmask. This option can be repeated multiple times within the same Interface Description Block when multiple IPv4 addresses are assigned to the interface. Note that the IP address and netmask are both treated as four octets, one for each octet of the address or mask; they are not 32-bit numbers, and thus the endianness of the SHB does not affect this field's value.</dd>

<dd style="margin-left: 8">Examples: '192 168 1 1 255 255 255 0'.</dd>

<dt>if_IPv6addr:</dt>

<dd style="margin-left: 8">  
The if_IPv6addr option is an IPv6 network address and corresponding prefix length for the interface. The first 16 octets are the IP address and the next octet is the prefix length. This option can be repeated multiple times within the same Interface Description Block when multiple IPv6 addresses are assigned to the interface.</dd>

<dd style="margin-left: 8">Example: 2001:0db8:85a3:08d3:1319:8a2e:0370:7344/64 is written (in hex) as '20 01 0d b8 85 a3 08 d3 13 19 8a 2e 03 70 73 44 40'.</dd>

<dt>if_MACaddr:</dt>

<dd style="margin-left: 8">  
The if_MACaddr option is the Interface Hardware MAC address (48 bits), if available.</dd>

<dd style="margin-left: 8">Example: '00 01 02 03 04 05'.</dd>

<dt>if_EUIaddr:</dt>

<dd style="margin-left: 8">  
The if_EUIaddr option is the Interface Hardware EUI address (64 bits), if available.</dd>

<dd style="margin-left: 8">Example: '02 34 56 FF FE 78 9A BC'.</dd>

<dt>if_speed:</dt>

<dd style="margin-left: 8">  
The if_speed option is a 64-bit number for the Interface speed (in bits per second).</dd>

<dd style="margin-left: 8">Example: the 64-bit decimal number 100000000 for 100Mbps.</dd>

<dt>if_tsresol:</dt>

<dd style="margin-left: 8">  
The if_tsresol option identifies the resolution of timestamps. If the Most Significant Bit is equal to zero, the remaining bits indicates the resolution of the timestamp as a negative power of 10 (e.g. 6 means microsecond resolution, timestamps are the number of microseconds since 1/1/1970). If the Most Significant Bit is equal to one, the remaining bits indicates the resolution as as negative power of 2 (e.g. 10 means 1/1024 of second). If this option is not present, a resolution of 10^-6 is assumed (i.e. timestamps have the same resolution of the standard 'libpcap' timestamps).</dd>

<dd style="margin-left: 8">Example: '6'.</dd>

<dt>if_tzone:</dt>

<dd style="margin-left: 8">  
The if_tzone option identifies the time zone for GMT support (TODO: specify better).</dd>

<dd style="margin-left: 8">Example: TODO: give a good example.</dd>

<dt>if_filter:</dt>

<dd style="margin-left: 8">  
The if_filter option is identifies the filter (e.g. "capture only TCP traffic") used to capture traffic. The first octet of the Option Data keeps a code of the filter used (e.g. if this is a libpcap string, or BPF bytecode, and more). More details about this format will be presented in Appendix XXX (TODO). (TODO: better use different options for different fields? e.g. if_filter_pcap, if_filter_bpf, ...)</dd>

<dd style="margin-left: 8">Example: '00'"tcp port 23 and host 192.0.2.5".</dd>

<dt>if_os:</dt>

<dd style="margin-left: 8">  
The if_os option is a UTF-8 string containing the name of the operating system of the machine in which this interface is installed. This can be different from the same information that can be contained by the Section Header Block ([Section 4.1](#section_shb)) because the capture can have been done on a remote machine.</dd>

<dd style="margin-left: 8">Examples: "Windows XP SP2", "openSUSE 10.2".</dd>

<dt>if_fcslen:</dt>

<dd style="margin-left: 8">  
The if_fcslen option is an 8-bit unsigned integer value that specifies the length of the Frame Check Sequence (in bits) for this interface. For link layers whose FCS length can change during time, the Enhanced Packet Block epb_flags Option can be used in each Enhanced Packet Block (see [Section 4.3.1](#section_epb_flags)).</dd>

<dd style="margin-left: 8">Example: '4'.</dd>

<dt>if_tsoffset:</dt>

<dd style="margin-left: 8">  
The if_tsoffset option is a 64-bit integer value that specifies an offset (in seconds) that must be added to the timestamp of each packet to obtain the absolute timestamp of a packet. If the option is missing, the timestamps stored in the packet MUST be considered absolute timestamps. The time zone of the offset can be specified with the option if_tzone. TODO: won't a if_tsoffset_low for fractional second offsets be useful for highly syncronized capture systems?</dd>

<dd style="margin-left: 8">Example: '1234'.</dd>

</dl>

# [4.3.](#rfc.section.4.3) [Enhanced Packet Block](#section_epb)

An Enhanced Packet Block (EPB) is the standard container for storing the packets coming from the network. The Enhanced Packet Block is optional because packets can be stored either by means of this block or the Simple Packet Block, which can be used to speed up capture file generation; or a file may have no packets in it. The format of an Enhanced Packet Block is shown in [Figure 11](#format_epb).

The Enhanced Packet Block is an improvement over the original, now obsolete, [Packet Block](#appendix_pb) <cite title="NONE">[appendix_pb]</cite>:

*   it stores the Interface Identifier as a 32-bit integer value. This is a requirement when a capture stores packets coming from a large number of interfaces
*   unlike the [Packet Block](#appendix_pb) <cite title="NONE">[appendix_pb]</cite>, the number of packets dropped by the capture system between this packet and the previous one is not stored in the header, but rather in an option of the block itself.

<div id="rfc.figure.11">

<div id="format_epb">

<pre>   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------------------------------------------------------+
 0 |                    Block Type = 0x00000006                    |
   +---------------------------------------------------------------+
 4 |                      Block Total Length                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 8 |                         Interface ID                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
12 |                        Timestamp (High)                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
16 |                        Timestamp (Low)                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
20 |                    Captured Packet Length                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
24 |                    Original Packet Length                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
28 /                                                               /
   /                          Packet Data                          /
   /              variable length, padded to 32 bits               /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   /                                                               /
   /                      Options (variable)                       /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Block Total Length                       |
   +---------------------------------------------------------------+
</pre>

Figure 11: Enhanced Packet Block Format

The Enhanced Packet Block has the following fields:

*   Block Type: The block type of the Enhanced Packet Block is 6.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Interface ID: it specifies the interface this packet comes from; the correct interface will be the one whose Interface Description Block (within the current Section of the file) is identified by the same number (see [Section 4.2](#section_idb)) of this field. The interface ID MUST be valid, which means that an matching interface description block MUST exist.
*   Timestamp (High) and Timestamp (Low): high and low 32-bits of a 64-bit quantity representing the timestamp. The timestamp is a single 64-bit unsigned integer representing the number of units since 1/1/1970 00:00:00 UTC. The way to interpret this field is specified by the 'if_tsresol' option (see [Figure 10](#format_idb)) of the Interface Description block referenced by this packet. Please note that differently from the libpcap file format, timestamps are not saved as two 32-bit values accounting for the seconds and microseconds since 1/1/1970\. They are saved as a single 64-bit quantity saved as two 32-bit words.
*   Captured Packet Length: number of octets captured from the packet (i.e. the length of the Packet Data field). It will be the minimum value among the Original Packet Length and the snapshot length for the interface (SnapLen, defined in [Figure 10](#format_idb)). The value of this field does not include the padding octets added at the end of the Packet Data field to align the Packet Data field to a 32-bit boundary.
*   Original Packet Length: actual length of the packet when it was transmitted on the network. It can be different from Captured Packet Length if the packet has been truncated by the capture process.
*   Packet Data: the data coming from the network, including link-layer headers. The actual length of this field is Captured Packet Length plus the padding to a 32-bit boundary. The format of the link-layer headers depends on the LinkType field specified in the Interface Description Block (see [Section 4.2](#section_idb)) and it is specified in the entry for that format in the [the tcpdump.org link-layer header types registry](http://www.tcpdump.org/linktypes.html).
*   Options: optionally, a list of options (formatted according to the rules defined in [Section 3.5](#section_opt)) can be present.

In addition to the options defined in [Section 3.5](#section_opt), the following options are valid within this block:

<div id="rfc.table.4">

<div id="options_epb">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Enhanced Packet Block Options</caption>

<thead>

<tr>

<th class="left">Name</th>

<th class="left">Code</th>

<th class="left">Length</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">epb_flags</td>

<td class="left">2</td>

<td class="left">4</td>

</tr>

<tr>

<td class="left">epb_hash</td>

<td class="left">3</td>

<td class="left">variable</td>

</tr>

<tr>

<td class="left">epb_dropcount</td>

<td class="left">4</td>

<td class="left">8</td>

</tr>

</tbody>

</table>

<dl>

<dt>epb_flags:</dt>

<dd style="margin-left: 8">  
The epb_flags option is a 32-bit flags word containing link-layer information. A complete specification of the allowed flags can be found in [Section 4.3.1](#section_epb_flags).</dd>

<dd style="margin-left: 8">Example: '0'.</dd>

<dt>epb_hash:</dt>

<dd style="margin-left: 8">  
The epb_hash option contains a hash of the packet. The first octet specifies the hashing algorithm, while the following octets contain the actual hash, whose size depends on the hashing algorithm, and hence from the value in the first octet. The hashing algorithm can be: 2s complement (algorithm octet = 0, size=XXX), XOR (algorithm octet = 1, size=XXX), CRC32 (algorithm octet = 2, size = 4), MD-5 (algorithm octet = 3, size=XXX), SHA-1 (algorithm octet = 4, size=XXX). The hash covers only the packet, not the header added by the capture driver: this gives the possibility to calculate it inside the network card. The hash allows easier comparison/merging of different capture files, and reliable data transfer between the data acquisition system and the capture library.</dd>

<dd style="margin-left: 8">Examples: '02 EC 1D 87 97', '03 45 6E C2 17 7C 10 1E 3C 2E 99 6E C2 9A 3D 50 8E'.</dd>

<dt>epb_dropcount:</dt>

<dd style="margin-left: 8">  
The epb_dropcount option is a 64-bit integer value specifying the number of packets lost (by the interface and the operating system) between this packet and the preceding one.</dd>

<dd style="margin-left: 8">Example: '0'.</dd>

</dl>

# [4.3.1.](#rfc.section.4.3.1) [Enhanced Packet Block Flags Word](#section_epb_flags)

The Enhanced Packet Block Flags Word is a 32-bit value that contains link-layer information about the packet.

The word is encoded as an unsigned 32-bit integer, using the endianness of the Section Header Block scope it is in. In the following table, the bits are numbered with 0 being the most- significant bit and 31 being the least-significant bit of the 32-bit unsigned integer. The meaning of the bits is the following:

<table cellpadding="3" cellspacing="0" class="tt full center">

<thead>

<tr>

<th class="left">Bit Number</th>

<th class="left">Description</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">0-1</td>

<td class="left">Inbound / Outbound packet (00 = information not available, 01 = inbound, 10 = outbound)</td>

</tr>

<tr>

<td class="left">2-4</td>

<td class="left">Reception type (000 = not specified, 001 = unicast, 010 = multicast, 011 = broadcast, 100 = promiscuous).</td>

</tr>

<tr>

<td class="left">5-8</td>

<td class="left">FCS length, in octets (0000 if this information is not available). This value overrides the if_fcslen option of the Interface Description Block, and is used with those link layers (e.g. PPP) where the length of the FCS can change during time.</td>

</tr>

<tr>

<td class="left">9-15</td>

<td class="left">Reserved (MUST be set to zero).</td>

</tr>

<tr>

<td class="left">16-31</td>

<td class="left">link-layer-dependent errors (Bit 31 = symbol error, Bit 30 = preamble error, Bit 29 = Start Frame Delimiter error, Bit 28 = unaligned frame error, Bit 27 = wrong Inter Frame Gap error, Bit 26 = packet too short error, Bit 25 = packet too long error, Bit 24 = CRC error, other?? are 16 bit enough?).</td>

</tr>

</tbody>

</table>

# [4.4.](#rfc.section.4.4) [Simple Packet Block](#section_spb)

The Simple Packet Block (SPB) is a lightweight container for storing the packets coming from the network. Its presence is optional.

A Simple Packet Block is similar to an Enhanced Packet Block (see [Section 4.3](#section_epb)), but it is smaller, simpler to process and contains only a minimal set of information. This block is preferred to the standard Enhanced Packet Block when performance or space occupation are critical factors, such as in sustained traffic capture applications. A capture file can contain both Enhanced Packet Blocks and Simple Packet Blocks: for example, a capture tool could switch from Enhanced Packet Blocks to Simple Packet Blocks when the hardware resources become critical.

The Simple Packet Block does not contain the Interface ID field. Therefore, it MUST be assumed that all the Simple Packet Blocks have been captured on the interface previously specified in the first Interface Description Block.

[Figure 12](#format_spb) shows the format of the Simple Packet Block.

<div id="rfc.figure.12">

<div id="format_spb">

<pre>    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------------------------------------------------------+
 0 |                    Block Type = 0x00000003                    |
   +---------------------------------------------------------------+
 4 |                      Block Total Length                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 8 |                    Original Packet Length                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
12 /                                                               /
   /                          Packet Data                          /
   /              variable length, padded to 32 bits               /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Block Total Length                       |
   +---------------------------------------------------------------+
 </pre>

Figure 12: Simple Packet Block Format

The Simple Packet Block has the following fields:

*   Block Type: The block type of the Simple Packet Block is 3.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Original Packet Length: actual length of the packet when it was transmitted on the network. It can be different from length of the Packet Data field's length if the packet has been truncated by the capture process, in which case the SnapLen value in [Section 4.2](#section_idb) will be less than this Original Packet Length value, and the SnapLen value MUST be used to determine the size of the Packet Data field length.
*   Packet Data: the data coming from the network, including link-layer headers. The length of this field can be derived from the field Block Total Length, present in the Block Header, and it is the minimum value among the SnapLen (present in the Interface Description Block) and the Original Packet Length (present in this header). The format of the data within this Packet Data field depends on the LinkType field specified in the Interface Description Block (see [Section 4.2](#section_idb)) and it is specified in the entry for that format in [the tcpdump.org link-layer header types registry](http://www.tcpdump.org/linktypes.html).

The Simple Packet Block does not contain the timestamp because this is often one of the most costly operations on PCs. Additionally, there are applications that do not require it; e.g. an Intrusion Detection System is interested in packets, not in their timestamp.

A Simple Packet Block cannot be present in a Section that has more than one interface because of the impossibility to refer to the correct one (it does not contain any Interface ID field).

The Simple Packet Block is very efficient in term of disk space: a snapshot whose length is 100 octets requires only 16 octets of overhead, which corresponds to an efficiency of more than 86%.

# [4.5.](#rfc.section.4.5) [Name Resolution Block](#section_nrb)

The Name Resolution Block (NRB) is used to support the correlation of numeric addresses (present in the captured packets) and their corresponding canonical names and it is optional. Having the literal names saved in the file prevents the need for performing name resolution at a later time, when the association between names and addresses may be different from the one in use at capture time. Moreover, the NRB avoids the need for issuing a lot of DNS requests every time the trace capture is opened, and also provides name resolution when reading the capture with a machine not connected to the network.

A Name Resolution Block is often placed at the beginning of the file, but no assumptions can be taken about its position. Multiple NRBs can exist in a pcapng file, either due to memory constraints or because additional name resolutions were performed by file processing tools, like network analyzers.

A Name Resolution Block need not contain any Records, except the nrb_record_end Record which MUST be the last Record. The addresses and names in NRB Records MAY be repeated multiple times; i.e., the same IP address may resolve to multiple names, the same name may resolve to the multiple IP addresses, and even the same address-to-name pair may appear multiple times, in the same NRB or across NRBs.

The format of the Name Resolution Block is shown in [Figure 13](#format_nrb).

<div id="rfc.figure.13">

<div id="format_nrb">

<pre>    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------------------------------------------------------+
 0 |                    Block Type = 0x00000004                    |
   +---------------------------------------------------------------+
 4 |                      Block Total Length                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 8 |      Record Type              |      Record Value Length      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
12 /                       Record Value                            /
   /              variable length, padded to 32 bits               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   .                                                               .
   .                  . . . other records . . .                    .
   .                                                               .
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Record Type = nrb_record_end |   Record Value Length = 0     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   /                                                               /
   /                      Options (variable)                       /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Block Total Length                       |
   +---------------------------------------------------------------+
 </pre>

Figure 13: Name Resolution Block Format

The Name Resolution Block has the following fields:

*   Block Type: The block type of the Name Resolution Block is 4.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).

This is followed by zero or more Name Resolution Records (in the TLV format), each of which contains an association between a network address and a name. An nrb_record_end MUST be added after the last Record, and MUST exist even if there are no other Records in the NRB. There are currently three possible types of records:

<div id="rfc.table.5">

<div id="nrrecords">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Name Resolution Block Records</caption>

<thead>

<tr>

<th class="left">Name</th>

<th class="left">Code</th>

<th class="left">Length</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">nrb_record_end</td>

<td class="left">0x0000</td>

<td class="left">0</td>

</tr>

<tr>

<td class="left">nrb_record_ipv4</td>

<td class="left">0x0001</td>

<td class="left">Variable</td>

</tr>

<tr>

<td class="left">nrb_record_ipv6</td>

<td class="left">0x0002</td>

<td class="left">Variable</td>

</tr>

</tbody>

</table>

<dl>

<dt>nrb_record_end:</dt>

<dd style="margin-left: 8">  
The nrb_record_end record delimits the end of name resolution records. This record is needed to determine when the list of name resolution records has ended and some options (if any) begin.</dd>

<dt>nrb_record_ipv4:</dt>

<dd style="margin-left: 8">  
The nrb_record_ipv4 record specifies an IPv4 address (contained in the first 4 octets), followed by one or more zero-terminated UTF-8 strings containing the DNS entries for that address. The minimum valid Record Length for this Record Type is thus 6: 4 for the IP octets, 1 character, and a zero-value octet terminator. Note that the IP address is treated as four octets, one for each octet of the IP address; it is not a 32-bit word, and thus the endianness of the SHB does not affect this field's value.</dd>

<dd style="margin-left: 8">Example: '127 0 0 1'"localhost".</dd>

<dd style="margin-left: 8">[Open issue: is an empty string (i.e., just a zero-value octet) valid?]</dd>

<dt>nrb_record_ipv6:</dt>

<dd style="margin-left: 8">  
The nrb_record_ipv6 record specifies an IPv6 address (contained in the first 16 octets), followed by one or more zero-terminated strings containing the DNS entries for that address. The minimum valid Record Length for this Record Type is thus 18: 16 for the IP octets, 1 character, and a zero-value octet terminator.</dd>

<dd style="margin-left: 8">Example: '20 01 0d b8 00 00 00 00 00 00 00 00 12 34 56 78'"somehost".</dd>

<dd style="margin-left: 8">[Open issue: is an empty string (i.e., just a zero-value octet) valid?]</dd>

</dl>

Record Types other than those specified earlier MUST be ignored and skipped past. More Record Types will likely be defined in the future, and MUST NOT break backwards compatibility.

Each Record Value is aligned to and padded to a 32-bit boundary. The corresponding Record Value Length reflects the actual length of the Record Value; it does not include the lengths of the Record Type field, the Record Value Length field, any padding for the Record Value, or anything after the Record Value. For Record Types with name strings, the Record Length does include the zero-value octet terminating that string. A Record Length of 0 is valid, unless indicated otherwise.

After the list of Name Resolution Records, optionally, a list of options (formatted according to the rules defined in [Section 3.5](#section_opt)) can be present.

In addition to the options defined in [Section 3.5](#section_opt), the following options are valid within this block:

<div id="rfc.table.6">

<div id="options_nrb">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Name Resolution Block Options</caption>

<thead>

<tr>

<th class="left">Name</th>

<th class="left">Code</th>

<th class="left">Length</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">ns_dnsname</td>

<td class="left">2</td>

<td class="left">Variable</td>

</tr>

<tr>

<td class="left">ns_dnsIP4addr</td>

<td class="left">3</td>

<td class="left">4</td>

</tr>

<tr>

<td class="left">ns_dnsIP6addr</td>

<td class="left">4</td>

<td class="left">16</td>

</tr>

</tbody>

</table>

<dl>

<dt>ns_dnsname:</dt>

<dd style="margin-left: 8">  
The ns_dnsname option is a UTF-8 string containing the name of the machine (DNS server) used to perform the name resolution.</dd>

<dd style="margin-left: 8">Example: "our_nameserver".</dd>

<dt>ns_dnsIP4addr:</dt>

<dd style="margin-left: 8">  
The ns_dnsIP4addr option specifies the IPv4 address of the DNS server. Note that the IP address is treated as four octets, one for each octet of the IP address; it is not a 32-bit word, and thus the endianness of the SHB does not affect this field's value.</dd>

<dd style="margin-left: 8">Example: '192 168 0 1'.</dd>

<dt>ns_dnsIP6addr:</dt>

<dd style="margin-left: 8">  
The ns_dnsIP6addr option specifies the IPv6 address of the DNS server.</dd>

<dd style="margin-left: 8">Example: '20 01 0d b8 00 00 00 00 00 00 00 00 12 34 56 78'.</dd>

</dl>

# [4.6.](#rfc.section.4.6) [Interface Statistics Block](#section_isb)

The Interface Statistics Block (ISB) contains the capture statistics for a given interface and it is optional. The statistics are referred to the interface defined in the current Section identified by the Interface ID field. An Interface Statistics Block is normally placed at the end of the file, but no assumptions can be taken about its position - it can even appear multiple times for the same interface.

The format of the Interface Statistics Block is shown in [Figure 14](#format_isb).

<div id="rfc.figure.14">

<div id="format_isb">

<pre>    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------------------------------------------------------+
 0 |                   Block Type = 0x00000005                     |
   +---------------------------------------------------------------+
 4 |                      Block Total Length                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 8 |                         Interface ID                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
12 |                        Timestamp (High)                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
16 |                        Timestamp (Low)                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
20 /                                                               /
   /                      Options (variable)                       /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Block Total Length                       |
   +---------------------------------------------------------------+
 </pre>

Figure 14: Interface Statistics Block Format

The fields have the following meaning:

*   Block Type: The block type of the Interface Statistics Block is 5.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Interface ID: specifies the interface these statistics refers to; the correct interface will be the one whose Interface Description Block (within the current Section of the file) is identified by same number (see [Section 4.2](#section_idb)) of this field.
*   Timestamp: time this statistics refers to. The format of the timestamp is the same already defined in the Enhanced Packet Block ([Section 4.3](#section_epb)).
*   Options: optionally, a list of options (formatted according to the rules defined in [Section 3.5](#section_opt)) can be present.

All the statistic fields are defined as options in order to deal with systems that do not have a complete set of statistics. Therefore, In addition to the options defined in [Section 3.5](#section_opt), the following options are valid within this block:

<div id="rfc.table.7">

<div id="options_isb">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Interface Statistics Block Options</caption>

<thead>

<tr>

<th class="left">Name</th>

<th class="left">Code</th>

<th class="left">Length</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">isb_starttime</td>

<td class="left">2</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">isb_endtime</td>

<td class="left">3</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">isb_ifrecv</td>

<td class="left">4</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">isb_ifdrop</td>

<td class="left">5</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">isb_filteraccept</td>

<td class="left">6</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">isb_osdrop</td>

<td class="left">7</td>

<td class="left">8</td>

</tr>

<tr>

<td class="left">isb_usrdeliv</td>

<td class="left">8</td>

<td class="left">8</td>

</tr>

</tbody>

</table>

<dl>

<dt>isb_starttime:</dt>

<dd style="margin-left: 8">  
The isb_starttime option specifies the time the capture started; time will be stored in two blocks of four octets each. The format of the timestamp is the same as the one defined in the Enhanced Packet Block ([Section 4.3](#section_epb)).</dd>

<dd style="margin-left: 8">Example: '97 c3 04 00 aa 47 ca 64' in Little Endian, decodes to 06/29/2012 06:16:50 UTC.</dd>

<dt>isb_endtime:</dt>

<dd style="margin-left: 8">  
The isb_endtime option specifies the time the capture ended; time will be stored in two blocks of four octets each. The format of the timestamp is the same as the one defined in the Enhanced Packet Block ([Section 4.3](#section_epb)).</dd>

<dd style="margin-left: 8">Example: '96 c3 04 00 73 89 6a 65', in Little Endian, decodes to 06/29/2012 06:17:00 UTC.</dd>

<dt>isb_ifrecv:</dt>

<dd style="margin-left: 8">  
The isb_ifrecv option specifies the 64-bit unsigned integer number of packets received from the physical interface starting from the beginning of the capture.</dd>

<dd style="margin-left: 8">Example: the decimal number 100.</dd>

<dt>isb_ifdrop:</dt>

<dd style="margin-left: 8">  
The isb_ifdrop option specifies the 64-bit unsigned integer number of packets dropped by the interface due to lack of resources starting from the beginning of the capture.</dd>

<dd style="margin-left: 8">Example: '0'.</dd>

<dt>isb_filteraccept:</dt>

<dd style="margin-left: 8">  
The isb_filteraccept option specifies the 64-bit unsigned integer number of packets accepted by filter starting from the beginning of the capture.</dd>

<dd style="margin-left: 8">Example: the decimal number 100.</dd>

<dt>isb_osdrop:</dt>

<dd style="margin-left: 8">  
The isb_osdrop option specifies the 64-bit unsigned integer number of packets dropped by the operating system starting from the beginning of the capture.</dd>

<dd style="margin-left: 8">Example: '0'.</dd>

<dt>isb_usrdeliv:</dt>

<dd style="margin-left: 8">  
The isb_usrdeliv option specifies the 64-bit unsigned integer number of packets delivered to the user starting from the beginning of the capture. The value contained in this field can be different from the value 'isb_filteraccept - isb_osdrop' because some packets could still be in the OS buffers when the capture ended.</dd>

<dd style="margin-left: 8">Example: '0'.</dd>

</dl>

All the fields that refer to packet counters are 64-bit values, represented with the octet order of the current section. Special care must be taken in accessing these fields: since all the blocks are aligned to a 32-bit boundary, such fields are not guaranteed to be aligned on a 64-bit boundary.

# [4.7.](#rfc.section.4.7) [Custom Block](#section_custom_block)

A Custom Block (CB) is the container for storing custom data that is not part of another block; for storing custom data as part of another block, see [Section 3.5.1](#section_custom_option). The Custom Block is optional, can be repeated any number of times, and can appear before or after any other block except the first Section Header Block which must come first in the file. Different Custom Blocks, of different type codes and/or different Private Enterprise Numbers, may be used in the same pcapng file. The format of a Custom Block is shown in [Figure 15](#format_custom_block).

<div id="rfc.figure.15">

<div id="format_custom_block">

<pre>   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------------------------------------------------------+
 0 |             Block Type = 0x00000BAD or 0x40000BAD             |
   +---------------------------------------------------------------+
 4 |                      Block Total Length                       |
   +---------------------------------------------------------------+
 8 |                Private Enterprise Number (PEN)                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
12 /                                                               /
   /                          Custom Data                          /
   /              variable length, padded to 32 bits               /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   /                                                               /
   /                      Options (variable)                       /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Block Total Length                       |
   +---------------------------------------------------------------+
</pre>

Figure 15: Custom Block Format

The Custom Block uses the type code 0x00000BAD (2989 in decimal) for a custom block that pcapng re-writers can copy into new files, and the type code 0x40000BAD (1073744813 in decimal) for one that should not be copied. See [Section 6.2](#section_vendor_copy) for details.

The Custom Block has the following fields:

*   Block Type: The block type of the Custom Block is 0x00000BAD or 0x40000BAD, as described previously.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Private Enterprise Number: An IANA-assigned Private Enterprise Number identifying the organization which defined the Custom Block. See [Section 6.1](#section_vendor_uses) for details. The PEN number MUST be encoded using the same endianness as the Section Header Block it is within the scope of.
*   Custom Data: the custom data, padded to a 32 bit boundary.
*   Options: optionally, a list of options (formatted according to the rules defined in [Section 3.5](#section_opt)) can be present. Note that custom options for the Custom Block still use the custom option format and type code, as described in [Section 3.5.1](#section_custom_option).

# [5.](#rfc.section.5) Experimental Blocks (deserve further investigation)

# [5.1.](#rfc.section.5.1) Alternative Packet Blocks (experimental)

Can some other packet blocks (besides the ones described in the previous paragraphs) be useful?

# [5.2.](#rfc.section.5.2) Compression Block (experimental)

The Compression Block is optional. A file can contain an arbitrary number of these blocks. A Compression Block, as the name says, is used to store compressed data. Its format is shown in [Figure 16](#formatcb).

<div id="rfc.figure.16">

<div id="formatcb">

<pre> 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+---------------------------------------------------------------+
|                        Block Type = ?                         |
+---------------------------------------------------------------+
|                      Block Total Length                       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|  Compr. Type  |                                               |
+-+-+-+-+-+-+-+-+                                               |
|                                                               |
|                       Compressed Data                         |
|                                                               |
|  variable length, octet-aligned and padded to end on a 32-bit |
|                         boundary                              |
|                                                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                      Block Total Length                       |
+---------------------------------------------------------------+
 </pre>

Figure 16: Compression Block Format

The fields have the following meaning:

*   Block Type: The block type of the Compression Block is not yet assigned.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Compression Type: specifies the compression algorithm. Possible values for this field are 0 (uncompressed), 1 (Lempel Ziv), 2 (Gzip), other?? Probably some kind of dumb and fast compression algorithm could be effective with some types of traffic (for example web), but which?
*   Compressed Data: data of this block. Once decompressed, it is made of other blocks.

# [5.3.](#rfc.section.5.3) Encryption Block (experimental)

The Encryption Block is optional. A file can contain an arbitrary number of these blocks. An Encryption Block is used to store encrypted data. Its format is shown in [Figure 17](#formateb).

<div id="rfc.figure.17">

<div id="formateb">

<pre> 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+---------------------------------------------------------------+
|                        Block Type = ?                         |
+---------------------------------------------------------------+
|                      Block Total Length                       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   Encr. Type  |                                               |
+-+-+-+-+-+-+-+-+                                               |
|                                                               |
|                       Encrypted Data                          |
|                                                               |
|                 variable length, octet-aligned                |
|                                                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                      Block Total Length                       |
+---------------------------------------------------------------+
 </pre>

Figure 17: Encryption Block Format

The fields have the following meaning:

*   Block Type: The block type of the Encryption Block is not yet assigned.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Encryption Type: specifies the encryption algorithm. Possible values for this field are ??? (TODO) NOTE: this block should probably contain other fields, depending on the encryption algorithm. To be defined precisely.
*   Encrypted Data: data of this block. Once decrypted, it originates other blocks.

# [5.4.](#rfc.section.5.4) Fixed Length Block (experimental)

The Fixed Length Block is optional. A file can contain an arbitrary number of these blocks. A Fixed Length Block can be used to optimize the access to the file. Its format is shown in [Figure 18](#formatflm). A Fixed Length Block stores records with constant size. It contains a set of Blocks (normally Enhanced Packet Blocks or Simple Packet Blocks), of which it specifies the size. Knowing this size a priori helps to scan the file and to load some portions of it without truncating a block, and is particularly useful with cell-based networks like ATM.

<div id="rfc.figure.18">

<div id="formatflm">

<pre> 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+---------------------------------------------------------------+
|                        Block Type = ?                         |
+---------------------------------------------------------------+
|                      Block Total Length                       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          Cell Size            |                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               |
|                                                               |
|                        Fixed Size Data                        |
|                                                               |
|                 variable length, octet-aligned                |
|                                                               |
|                                                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                      Block Total Length                       |
+---------------------------------------------------------------+
 </pre>

Figure 18: Fixed Length Block Format

The fields have the following meaning:

*   Block Type: The block type of the Fixed Length Block is not yet assigned.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Cell size: the size of the blocks contained in the data field.
*   Fixed Size Data: data of this block.

# [5.5.](#rfc.section.5.5) Directory Block (experimental)

If present, this block contains the following information:

*   number of indexed packets (N)
*   table with position and length of any indexed packet (N entries)

A directory block MUST be followed by at least N packets, otherwise it MUST be considered invalid. It can be used to efficiently load portions of the file to memory and to support operations on memory mapped files. This block can be added by tools like network analyzers as a consequence of file processing.

# [5.6.](#rfc.section.5.6) Traffic Statistics and Monitoring Blocks (experimental)

One or more blocks could be defined to contain network statistics or traffic monitoring information. They could be use to store data collected from RMON or Netflow probes, or from other network monitoring tools.

# [5.7.](#rfc.section.5.7) Event/Security Block (experimental)

This block could be used to store events. Events could contain generic information (for example network load over 50%, server down...) or security alerts. An event could be:

*   skipped, if the application doesn't know how to do with it
*   processed independently by the packets. In other words, the applications skips the packets and processes only the alerts
*   processed in relation to packets: for example, a security tool could load only the packets of the file that are near a security alert; a monitoring tool could skip the packets captured while the server was down.

# [6.](#rfc.section.6) [Vendor-Specific Custom Extensions](#section_vendor)

This section uses the term "vendor" to describe an organization which extends the pcapng file with custom, proprietary blocks or options. It should be noted, however, that the "vendor" is just an abstract entity that agrees on a custom extension format: for example it may be a manufacturer, industry association, an individual user, or collective group of users.

# [6.1.](#rfc.section.6.1) [Supported Use-Cases](#section_vendor_uses)

There are two different supported use-cases for vendor-specific custom extensions: local and portable. Local use means the custom data is only expected to be usable on the same machine, and the same application, which encoded it into the file. This limitation is due to the lack of a common registry for the local use number codes (the block or option type code numbers with the Most Significant Bit set). Since two different vendors may choose the same number, one vendor's application reading the other vendor's file would result in decoding failure. Therefore, vendors SHOULD instead use the portable method, as described next.

The portable use-case supports vendor-specific custom extensions in pcapng files which can be shared across systems, organizations, etc. To avoid number space collisions, an IANA-registered Private Enterprise Number (PEN) is encoded into the Custom Block or Custom Option, using the PEN number that belongs to the vendor defining the extension. Anyone can register a new PEN with IANA, for free, by filling out the online request form at [http://pen.iana.org/pen/PenApplication.pag e](http://pen.iana.o         rg/pen/PenApplication.page).

# [6.2.](#rfc.section.6.2) [Controlling Copy Behavior](#section_vendor_copy)

Both Custom Blocks and Custom Options support two different codes to distinguish their "copy" behavior: a code for when the block or option can be safely copied into a new pcapng file by a pcapng manipulatign application, and a code for when it should not be copied. A common reason for not copying a Custom Block or Custom Option is because it depends on other blocks or options in some way that would invalidate the custom data if the other blocks/options were removed or re-ordered. For example, if a Custom Block's data includes an Interface ID number in its Custom Data portion, then it cannot be safely copied by a pcapng application that merges pcapng files, because the merging application might re-order or remove one or more of the Interface Description Blocks, and thereby change the Interface IDs that the Custom Block depends upon. The same issue arises if a Custom Block or Custom Option depends on the presence of, or specific ordering of, other standard-based or custom-defined blocks or options.

Note that the copy semantics is not related to privacy - there is no guarantee that a pcapng anonymizer will remove a Custom Block or Custom Option, even if the appropriate code is used requesting it not be copied; and the original pcapng file can be shared anyway. If the Custom Data portion of the Custom Block or Custom Option contains sensitive information, then it should be encrypted in some fashion.

# [6.3.](#rfc.section.6.3) [Strings vs. Bytes](#section_vendor_strings)

For the Custom Options, there are two Custom Data formats supported: a UTF-8 string and a binary data payload. The rationale for this separation is that a pcapng display appplication which does not understand the specific PEN's Custom Option can still display the data as a string if it's a string type code, rather than as hex-ascii of the octets.

# [6.4.](#rfc.section.6.4) [Endianness Issues](#section_vendor_endian)

Implementers writing Custom Blocks or Custom Options should be aware that a pcapng file can be re-written by machines using a different endianness than the original file, which means all known fields of the pcapng file will change endianness in the new file. Since the Custom Data payload of the Custom Block or Custom Option might be an arbitrary sequence of unknown octets to such machines, they cannot convert multi-byte values inside the Custom Data into the appropriate endianness.

For example, a little-endian machine can create a new pcapng file and add some binary data Custom Options to some Block(s) in the file. This file can then be sent to a big-endian host, which will convert it to big-endian format if it re-writes the file. It will, however, leave the Custom Data payload alone (as little-endian format). If this file then gets sent back to the little-endian machine, then when that little-endian machine reads the file it will detect the format is big- endian, and swap the endianness while it parses the file - but that will cause the Custom Data payload to be incorrect since it was already in little-endian format.

Therefore, the vendor should either encode all of their fields in a consistent manner, such as always in big-endian or always little- endian format, regardless of the host platform's endianness; or they should encode some flag in the Custom Data payload to indicate which endianness the rest of the payload is written in.

# [7.](#rfc.section.7) Recommended File Name Extension: .pcapng

The recommended file name extension for the "PCAP Next Generation Capture File Format" specified in this document is ".pcapng".

On Windows and OS X, files are distinguished by an extension to their filename. Such an extension is technically not actually required, as applications should be able to automatically detect the pcapng file format through the "magic bytes" at the beginning of the file, as some other UN*X desktop environments do. However, using name extensions makes it easier to work with files (e.g. visually distinguish file formats) so it is recommended - though not required - to use .pcapng as the name extension for files following this specification.

Please note: To avoid confusion (like the current usage of .cap for a plethora of different capture file formats) other file name extensions than .pcapng should be avoided.

# [8.](#rfc.section.8) Conclusions

The file format proposed in this document should be very versatile and satisfy a wide range of applications. In the simplest case, it can contain a raw capture of the network data, made of a series of Simple Packet Blocks. In the most complex case, it can be used as a repository for heterogeneous information. In every case, the file remains easy to parse and an application can always skip the data it is not interested in; at the same time, different applications can share the file, and each of them can benefit of the information produced by the others. Two or more files can be concatenated obtaining another valid file.

# [9.](#rfc.section.9) Implementations

Some known implementations that read or write the pcapng file format are listed on the [pcapng GitHub wiki](https://github.com/pcapng/pcapng/wiki/Implementations).

# [10.](#rfc.section.10) Security Considerations

TBD.

# [11.](#rfc.section.11) IANA Considerations

TBD.

[Open issue: decide whether the block types, option types, NRB Record types, etc. should be IANA registries. And if so, what the IANA policy for each should be (see RFC 5226)]

# [11.1.](#rfc.section.11.1) [Standardized Block Type Codes](#section_block_code_registry)

Every Block is uniquely identified by a 32-bit integer value, stored in the Block Header.

As pointed out in [Section 3.1](#section_block), Block Type codes whose Most Significant Bit (bit 31) is set to 1 are reserved for local use by the application.

All the remaining Block Type codes (0x00000000 to 0x7FFFFFFF) are standardized by this document. Requests for new Block Type codes should be sent to the [pcap- ng-format mailing list](https://www.winpcap.org/mailman/listinfo/pcap-ng-format).

The following is a list of the Standardized Block Type Codes:

<div id="rfc.table.8">

<div id="blockcodes">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Standardized Block Type Codes</caption>

<thead>

<tr>

<th class="left">Block Type Code</th>

<th class="left">Description</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">0x00000000</td>

<td class="left">Reserved ???</td>

</tr>

<tr>

<td class="left">0x00000001</td>

<td class="left">[Interface Description Block](#section_idb) <cite title="NONE">[section_idb]</cite></td>

</tr>

<tr>

<td class="left">0x00000002</td>

<td class="left">[Packet Block](#appendix_pb) <cite title="NONE">[appendix_pb]</cite></td>

</tr>

<tr>

<td class="left">0x00000003</td>

<td class="left">[Simple Packet Block](#section_spb) <cite title="NONE">[section_spb]</cite></td>

</tr>

<tr>

<td class="left">0x00000004</td>

<td class="left">[Name Resolution Block](#section_nrb) <cite title="NONE">[section_nrb]</cite></td>

</tr>

<tr>

<td class="left">0x00000005</td>

<td class="left">[Interface Statistics Block](#section_isb) <cite title="NONE">[section_isb]</cite></td>

</tr>

<tr>

<td class="left">0x00000006</td>

<td class="left">[Enhanced Packet Block](#section_epb) <cite title="NONE">[section_epb]</cite></td>

</tr>

<tr>

<td class="left">0x00000007</td>

<td class="left">IRIG Timestamp Block (requested by Gianluca Varenni <gianluca.varenni@cacetech.com>, CACE Technologies LLC)</td>

</tr>

<tr>

<td class="left">0x00000008</td>

<td class="left">[ARINC 429](https://en.wikipedia.org/wiki/ARINC_429) in AFDX Encapsulation Information Block (requested by Gianluca Varenni <gianluca.varenni@cacetech.com>, CACE Technologies LLC)</td>

</tr>

<tr>

<td class="left">0x00000BAD</td>

<td class="left">[Custom Block that rewriters can copy into new files](#section_custom_block) <cite title="NONE">[section_custom_block]</cite></td>

</tr>

<tr>

<td class="left">0x40000BAD</td>

<td class="left">[Custom Block that rewriters should not copy into new files](#section_custom_block) <cite title="NONE">[section_custom_block]</cite></td>

</tr>

<tr>

<td class="left">0x0A0D0D0A</td>

<td class="left">[Section Header Block](#section_shb) <cite title="NONE">[section_shb]</cite></td>

</tr>

<tr>

<td class="left">0x0A0D0A00-0x0A0D0AFF</td>

<td class="left">Reserved. Used to detect trace files corrupted because of file transfers using the HTTP protocol in text mode.</td>

</tr>

<tr>

<td class="left">0x000A0D0A-0xFF0A0D0A</td>

<td class="left">Reserved. Used to detect trace files corrupted because of file transfers using the HTTP protocol in text mode.</td>

</tr>

<tr>

<td class="left">0x000A0D0D-0xFF0A0D0D</td>

<td class="left">Reserved. Used to detect trace files corrupted because of file transfers using the HTTP protocol in text mode.</td>

</tr>

<tr>

<td class="left">0x0D0D0A00-0x0D0D0AFF</td>

<td class="left">Reserved. Used to detect trace files corrupted because of file transfers using the FTP protocol in text mode.</td>

</tr>

<tr>

<td class="left">0x80000000-0xFFFFFFFF</td>

<td class="left">Reserved for local use.</td>

</tr>

</tbody>

</table>

[Open issue: reserve 0x40000000-0x7FFFFFFF for do-not-copy-bit range of base types?]

# [12.](#rfc.section.12) Contributors

Loris Degioanni and Gianluca Varenni were coauthoring this document before it was submitted to the IETF.

# [13.](#rfc.section.13) Acknowledgments

The authors wish to thank Anders Broman, Ulf Lamping, Richard Sharpe and many others for their invaluable comments.

# [14.](#rfc.references) Normative References

<table>

<tbody>

<tr>

<td class="reference">**[RFC2119]**</td>

<td class="top"><a>Bradner, S.</a>, "[Key words for use in RFCs to Indicate Requirement Levels](http://tools.ietf.org/html/rfc2119)", BCP 14, RFC 2119, DOI 10.17487/RFC2119, March 1997.</td>

</tr>

</tbody>

</table>

# [Appendix A.](#rfc.appendix.A) [Packet Block (obsolete!)](#appendix_pb)

The Packet Block is obsolete, and MUST NOT be used in new files. Use the Enhanced Packet Block or Simple Packet Block instead. This section is for historical reference only.

A Packet Block was a container for storing packets coming from the network.

<div id="rfc.figure.19">

<div id="formatpb">

<pre>    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------------------------------------------------------+
 0 |                    Block Type = 0x00000002                    |
   +---------------------------------------------------------------+
 4 |                      Block Total Length                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 8 |         Interface ID          |          Drops Count          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
12 |                        Timestamp (High)                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
16 |                        Timestamp (Low)                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
20 |                    Captured Packet Length                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
24 |                    Original Packet Length                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
28 /                                                               /
   /                          Packet Data                          /
   /              variable length, padded to 32 bits               /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   /                                                               /
   /                      Options (variable)                       /
   /                                                               /
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Block Total Length                       |
   +---------------------------------------------------------------+
</pre>

Figure 19: Packet Block Format

The Packet Block has the following fields:

*   Block Type: The block type of the Packet Block is 2.
*   Block Total Length: total size of this block, as described in [Section 3.1](#section_block).
*   Interface ID: specifies the interface this packet comes from; the correct interface will be the one whose Interface Description Block (within the current Section of the file) is identified by the same number (see [Section 4.2](#section_idb)) of this field. The interface ID MUST be valid, which means that an matching interface description block MUST exist.
*   Drops Count: a local drop counter. It specifies the number of packets lost (by the interface and the operating system) between this packet and the preceding one. The value xFFFF (in hexadecimal) is reserved for those systems in which this information is not available.
*   Timestamp (High) and Timestamp (Low): timestamp of the packet. The format of the timestamp is the same as was already defined for the Enhanced Packet Block ([Section 4.3](#section_epb)).
*   Captured Packet Length: number of octets captured from the packet (i.e. the length of the Packet Data field). It will be the minimum value among the Original Packet Length and the snapshot length for the interface (SnapLen, defined in [Figure 10](#format_idb)). The value of this field does not include the padding octets added at the end of the Packet Data field to align the Packet Data field to a 32-bit boundary.
*   Original Packet Length: actual length of the packet when it was transmitted on the network. It can be different from Captured Packet Length if the packet has been truncated by the capture process.
*   Packet Data: the data coming from the network, including link-layer headers. The actual length of this field is Captured Packet Length plus the padding to a 32-bit boundary. The format of the link-layer headers depends on the LinkType field specified in the Interface Description Block (see [Section 4.2](#section_idb)) and it is specified in the entry for that format in the [the tcpdump.org link-layer header types registry](http://www.tcpdump.org/linktypes.html).
*   Options: optionally, a list of options (formatted according to the rules defined in [Section 3.5](#section_opt)) can be present.

In addition to the options defined in [Section 3.5](#section_opt), the following options were valid within this block:

<div id="rfc.table.9">

<div id="optionspb">

<table cellpadding="3" cellspacing="0" class="tt full center"><caption>Packet Block Options</caption>

<thead>

<tr>

<th class="left">Name</th>

<th class="left">Code</th>

<th class="left">Length</th>

</tr>

</thead>

<tbody>

<tr>

<td class="left">pack_flags</td>

<td class="left">2</td>

<td class="left">4</td>

</tr>

<tr>

<td class="left">pack_hash</td>

<td class="left">3</td>

<td class="left">variable</td>

</tr>

</tbody>

</table>

<dl>

<dt>pack_flags:</dt>

<dd style="margin-left: 8">  
The pack_flags option is the same as the epb_flags of the enhanced packet block.</dd>

<dd style="margin-left: 8">Example: '0'.</dd>

<dt>pack_hash:</dt>

<dd style="margin-left: 8">  
The pack_hash option is the same as the epb_hash of the enhanced packet block.</dd>

<dd style="margin-left: 8">Examples: '02 EC 1D 87 97', '03 45 6E C2 17 7C 10 1E 3C 2E 99 6E C2 9A 3D 50 8E'.</dd>

</dl>

# [Authors' Addresses](#rfc.authors)

<div class="avoidbreak">

<address class="vcard"><span class="vcardline"><span class="fn">Michael Tuexen</span> (editor) <span class="n hidden"><span class="family-name">Tuexen</span></span> </span><span class="org vcardline">Muenster University of Applied Sciences</span> <span class="adr"><span class="vcardline">Stegerwaldstrasse 39</span> <span class="vcardline"><span class="locality">Steinfurt</span>, <span class="region"></span><span class="code">48565</span></span> <span class="country-name vcardline">DE</span></span> <span class="vcardline">EMail: [tuexen@fh-muenster.de](mailto:tuexen@fh-muenster.de)</span></address>

</div>

<div class="avoidbreak">

<address class="vcard"><span class="vcardline"><span class="fn">Fulvio Risso</span> <span class="n hidden"><span class="family-name">Risso</span></span> </span><span class="org vcardline">Politecnico di Torino</span> <span class="adr"><span class="vcardline">Corso Duca degli Abruzzi, 24</span> <span class="vcardline"><span class="locality">Torino</span>, <span class="region"></span><span class="code">10129</span></span> <span class="country-name vcardline">IT</span></span> <span class="vcardline">EMail: [fulvio.risso@polito.it](mailto:fulvio.risso@polito.it)</span></address>

</div>

<div class="avoidbreak">

<address class="vcard"><span class="vcardline"><span class="fn">Jasper Bongertz</span> <span class="n hidden"><span class="family-name">Bongertz</span></span> </span><span class="org vcardline">Airbus Defence and Space CyberSecurity</span> <span class="adr"><span class="vcardline">Kanzlei 63c</span> <span class="vcardline"><span class="locality">Meerbusch</span>, <span class="region"></span><span class="code">40667</span></span> <span class="country-name vcardline">DE</span></span> <span class="vcardline">EMail: [jasper@packet-foo.com](mailto:jasper@packet-foo.com)</span></address>

</div>

<div class="avoidbreak">

<address class="vcard"><span class="vcardline"><span class="fn">Gerald Combs</span> <span class="n hidden"><span class="family-name">Combs</span></span> </span><span class="org vcardline">Wireshark Foundation</span> <span class="adr"><span class="vcardline">339 Madson Pl</span> <span class="vcardline"><span class="locality">Davis</span>, <span class="region">CA</span> <span class="code">95618</span></span> <span class="country-name vcardline">US</span></span> <span class="vcardline">EMail: [gerald@wireshark.org](mailto:gerald@wireshark.org)</span></address>

</div>

<div class="avoidbreak">

<address class="vcard"><span class="vcardline"><span class="fn">Guy Harris</span> <span class="n hidden"><span class="family-name">Harris</span></span> </span><span class="org vcardline"></span><span class="adr"><span class="vcardline"><span class="locality"></span><span class="region"></span><span class="code"></span></span><span class="country-name vcardline"></span></span><span class="vcardline">EMail: [guy@alum.mit.edu](mailto:guy@alum.mit.edu)</span></address>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>