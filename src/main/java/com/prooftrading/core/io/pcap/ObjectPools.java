package com.prooftrading.core.io.pcap;

import nf.fr.eraasoft.pool.ObjectPool;
import nf.fr.eraasoft.pool.PoolException;
import nf.fr.eraasoft.pool.PoolSettings;
import nf.fr.eraasoft.pool.PoolableObject;

import java.util.concurrent.Callable;

@SuppressWarnings({"unused", "WeakerAccess"})
public class ObjectPools {

    public interface BasicObjectPool<T> {
        T newInstance();

        void returnInstance(T instance);

        void shutdown();
    }


    public static class StandardObjectPool<T> implements BasicObjectPool<T> {
        private final ObjectPool<T> pool;

        public StandardObjectPool(final Callable<T> factoryMethod) {
            PoolableObject<T> poolableObject = new PoolableObject<>() {
                @Override
                public T make() throws PoolException {
                    try {
                        return factoryMethod.call();
                    } catch (Exception e) {
                        throw new PoolException("Could not create new instance", e);
                    }
                }

                @Override
                public boolean validate(T t) {
                    return false;
                }

                @Override
                public void destroy(T t) {}

                @Override
                public void activate(T t) {}

                @Override
                public void passivate(T t) {}
            };
            PoolSettings<T> poolSettings = new PoolSettings<>(poolableObject);
            this.pool = poolSettings.pool();
        }

        public T newInstance() {
            try {
                return this.pool.getObj();
            } catch (PoolException e) {
                throw new IllegalStateException(e);
            }
        }

        public void returnInstance(T instance) {
            this.pool.returnObj(instance);
        }

        public void shutdown(){
            PoolSettings.shutdown();
        }

    }

    public static class SwapPool<T> implements ObjectPools.BasicObjectPool<T> {
        private T inst1;
        private T inst2;
        private T next;

        public SwapPool(Callable<T> factoryMethod) {
            try {
                this.inst1 = factoryMethod.call();
                this.inst2 = factoryMethod.call();
                this.next = this.inst1;
            } catch (Exception e) {
                throw new IllegalStateException("Could not create instances", e);
            }

        }

        public T newInstance(){
            T res = this.next;
            this.next = res == this.inst1 ? this.inst2 : this.inst1;
            return res;
        }

        public void returnInstance(T instance) {}

        public void shutdown() {}
    }
}
