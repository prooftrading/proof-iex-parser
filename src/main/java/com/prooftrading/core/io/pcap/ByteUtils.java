package com.prooftrading.core.io.pcap;

import java.nio.ByteBuffer;

public class ByteUtils {

    public static long getUInt(ByteBuffer bb, int index) {
        int v = bb.getInt(index);
        return ((long) v) & 0x00000000_FFFFFFFFL;
    }

    public static int getUShort(ByteBuffer bb, int index) {
        int v = bb.getShort(index);
        return v & 0x0000FFFF;
    }

    static int wordAlign(int n, int wordSize) {
        int mod = n % wordSize;
        return mod == 0 ? n : n + (wordSize - mod);
    }

}
