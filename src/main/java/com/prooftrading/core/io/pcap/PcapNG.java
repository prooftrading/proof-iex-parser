package com.prooftrading.core.io.pcap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import java.util.NoSuchElementException;

@SuppressWarnings({"unused", "WeakerAccess"})
public class PcapNG implements Iterable<PcapNG.Block>, AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(PcapNG.class);

    private final File file;
    private final FileChannel fileChannel;
    private final long fileSize;
    private final long desiredMapSize;
    private final BlockPool blockPool = new BlockPool();
    private final BlockIterator iterator;

    private long filePosition = 0;
    private int mapPosition;
    private long mapRemapThreshold;
    private MappedByteBuffer mbb;

    private ByteOrder currentByteOrder;
    private int currentLinkType;
    private long currentTimestampMultiplier = 1;

    public PcapNG(File file) throws IOException {
        this(file, 1024 * 1024 * 1024);
    }

    public PcapNG(File file, int desiredMapSize) throws IOException {
        this.file = file;
        this.fileChannel = FileChannel.open(file.toPath());
        this.fileSize = this.fileChannel.size();
        this.desiredMapSize = desiredMapSize;

        this.mapFile();
        this.mapRemapThreshold = this.mbb.remaining() - 9000 * 10;
        this.currentByteOrder = this.mbb.order();

        this.iterator = new BlockIterator();
    }

    private void mapFile() {
        try {
            long newMapSize = Math.min(desiredMapSize, this.fileSize - this.filePosition);
            LOG.info(String.format("mapFile(%s): range=[%d-%d](%.6f}, remaining=%.6f",
                    this.file, this.filePosition, this.filePosition + newMapSize, newMapSize / 1_000_000d,
                    (this.fileSize - this.filePosition) / 1_000_000d));
            this.mbb = this.fileChannel.map(FileChannel.MapMode.READ_ONLY, this.filePosition, newMapSize);
            LOG.trace("mapped: {} ({})", mbb, newMapSize);
            this.mapPosition = 0;
        } catch (IOException e) {
            throw new IllegalStateException("Could not map next section of file", e);
        }
    }

    @Override
    public void close() throws IOException {
        if (this.fileChannel != null) {
            this.fileChannel.close();
        }
        this.blockPool.shutdown();
    }

    private Block readNextBlock() {
        Block block = null;

        if (this.filePosition < this.fileSize) {
            // setup for next read
            this.mbb.limit(mbb.capacity()).position(0);
            this.mbb.order(this.currentByteOrder);

            // read and parse fields
            int blockType = this.mbb.getInt(this.mapPosition);
            block = this.blockPool.getBlock(blockType);
            block.init(this.mbb, blockType, this.mapPosition);

            // save/update state needed for reading
            if (block.getType() == SectionHeaderBlock.TYPE) {
                this.currentByteOrder = ((SectionHeaderBlock) block).getOrder();
            } else if (block.getType() == InterfaceBlock.TYPE) {
                InterfaceBlock interfaceBlock = (InterfaceBlock) block;
                this.currentTimestampMultiplier = interfaceBlock.getTimestampConversionToNanos();
                this.currentLinkType = interfaceBlock.getLinkType();
            }
            this.mapPosition += block.getLength();
            this.filePosition += block.getLength();

            // remap file, if needed
            if (this.mapPosition >= this.mapRemapThreshold) {
                mapFile();
            }
        }

        return block;
    }

    private void returnBlock(Block block) {
        this.blockPool.returnBlock(block);
    }

    @Override
    public Iterator<Block> iterator() {
        return this.iterator;
    }

    public File getFile() {
        return file;
    }

    public ByteOrder getCurrentByteOrder() {
        return currentByteOrder;
    }

    public int getCurrentLinkType() {
        return currentLinkType;
    }

    public long getCurrentTimestampMultiplier() {
        return currentTimestampMultiplier;
    }

    static class BlockPool {
        private final SectionHeaderBlock sectionHeaderBlock = new SectionHeaderBlock();
        private final InterfaceBlock interfaceBlock = new InterfaceBlock();
        private final ObjectPools.SwapPool<BaseBlock> unknownBlockPool = new ObjectPools.SwapPool<>(BaseBlock::new);
        private final ObjectPools.SwapPool<EnhancedPacketBlock> enhancedPacketBlockPool =
                new ObjectPools.SwapPool<>(EnhancedPacketBlock::new);

        public Block getBlock(int blockType) {
            switch (blockType) {
                case SectionHeaderBlock.TYPE:
                    return this.sectionHeaderBlock;
                case InterfaceBlock.TYPE:
                    return this.interfaceBlock;
                case EnhancedPacketBlock.TYPE:
                    return this.enhancedPacketBlockPool.newInstance();
                default:
                    // LOG.warn("Unsupported Block Type, returning un-typed block: type={}", blockType);
                    return unknownBlockPool.newInstance();
            }
        }

        public void returnBlock(Block block) {
            if (block == null) {
                return;
            }

            switch (block.getType()) {
                case SectionHeaderBlock.TYPE:
                case InterfaceBlock.TYPE:
                    break;
                case EnhancedPacketBlock.TYPE:
                    this.enhancedPacketBlockPool.returnInstance((EnhancedPacketBlock) block);
                    break;
                default:
                    this.unknownBlockPool.returnInstance((BaseBlock) block);
                    break;
            }
        }

        public void shutdown() {
            this.unknownBlockPool.shutdown();
        }
    }

    /**
     * Iterate blocks in a PcapNG File
     */
    class BlockIterator implements Iterator<Block> {
        private Block current = null;
        private Block next;

        BlockIterator() {
            this.next = readNextBlock();
        }

        @Override
        public boolean hasNext() {
            return this.next != null;
        }

        @Override
        public Block next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            returnBlock(this.current);
            this.current = this.next;
            this.next = readNextBlock();
            return this.current;
        }

    }

    interface Block {
        Block init(ByteBuffer bb, int type, int offset);

        int getType();

        long getLength();
    }

    public static class BaseBlock implements Block {
        protected ByteBuffer byteBuffer;
        protected int offset;

        protected int type;
        protected long length;

        @Override
        public Block init(ByteBuffer bb, int type, int offset) {
            this.byteBuffer = bb;
            this.type = type;
            this.offset = offset;

            this.length = ByteUtils.getUInt(bb, offset + 4);
            long length2 = ByteUtils.getUInt(bb, (int) (offset + this.length - 4));
            if (this.length != length2) {
                throw new IllegalStateException(
                        "Block Total Length mismatch:  beginning=" + this.length + ", end=" + length2);
            }
            return this;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public long getLength() {
            return length;
        }

        public ByteBuffer getByteBuffer() {
            return byteBuffer;
        }

        public int getOffset() {
            return offset;
        }

        protected void parseOptions(ByteBuffer bb, int startOffset, int optionsLen) {
            if (optionsLen <= 0) {
                return;
            }
            for (int i = startOffset, n = startOffset + optionsLen; i < n; ) {
                int optionType = ByteUtils.getUShort(bb, i);
                int optionLen = ByteUtils.getUShort(bb, i + 2);
                int alignedLen = ByteUtils.wordAlign(optionLen, 4);
                this.onOption(optionType, optionLen, bb, i + 4);
                i += alignedLen + 4;
                if (LOG.isDebugEnabled()) {
                    LOG.debug("parseOption: type={}, len={}({}), i={}, n={}", optionType, alignedLen, optionLen, i, n);
                }
            }
        }

        protected void onOption(int optionType, int optionLen, ByteBuffer bb, int offset) {
            LOG.info("onOption: type={}, len={}, offset={}", optionType, optionLen, offset);
        }

        @Override
        public String toString() {
            return "BaseBlock{" +
                    "byteBuffer=" + byteBuffer +
                    ", offset=" + offset +
                    ", type=" + type +
                    ", length=" + length +
                    '}';
        }
    }

    public static class SectionHeaderBlock extends BaseBlock {
        public static final int TYPE = 0x0A0D0D0A;

        private ByteOrder order;
        private int majorVersion;
        private int minorVersion;
        private long sectionLength;

        @Override
        public Block init(ByteBuffer bb, int type, int offset) {
            // read and set byte order first
            this.order = getByteOrder(bb, offset + 8);
            bb.order(order);

            super.init(bb, type, offset);
            this.majorVersion = ByteUtils.getUShort(bb, offset + 12);
            this.minorVersion = ByteUtils.getUShort(bb, offset + 14);
            this.sectionLength = bb.getLong(offset + 16);
            return this;
        }

        public ByteOrder getOrder() {
            return order;
        }

        public int getMajorVersion() {
            return majorVersion;
        }

        public int getMinorVersion() {
            return minorVersion;
        }

        public long getSectionLength() {
            return sectionLength;
        }

        @Override
        public String toString() {
            return "SectionHeaderBlock{" +
                    "byteBuffer=" + byteBuffer +
                    ", offset=" + offset +
                    ", type=" + type +
                    ", length=" + length +
                    ", order=" + order +
                    ", majorVersion=" + majorVersion +
                    ", minorVersion=" + minorVersion +
                    ", sectionLength=" + sectionLength +
                    '}';
        }

        public static ByteOrder getByteOrder(ByteBuffer buffer, int offset) {
            String byteMagic = String.format(
                    "%02x%02x%02x%02x",
                    buffer.get(offset),
                    buffer.get(offset + 1),
                    buffer.get(offset + 2),
                    buffer.get(offset + 3));
            ByteOrder bo = "4d3c2b1a".equals(byteMagic) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN;
            LOG.info("ByteOrder={} (byteMagic={})", bo, byteMagic);
            return bo;
        }
    }

    public static class InterfaceBlock extends BaseBlock {
        public static final int TYPE = 0x00000001;

        private int linkType;
        private long snapLen;
        private StringBuilder interfaceName = new StringBuilder();
        private byte timestampResolution = 0;
        private long timestampConversionToNanos = 0;

        @Override
        public Block init(ByteBuffer bb, int type, int offset) {
            super.init(bb, type, offset);
            this.linkType = ByteUtils.getUShort(bb, offset + 8);
            this.snapLen = ByteUtils.getUInt(bb, offset + 12);

            int optionsLen = (int) (this.length - 20);
            parseOptions(bb, offset + 16, optionsLen);

            return this;
        }

        protected void onOption(int optionType, int optionLen, ByteBuffer bb, int offset) {
            Object val = null;
            if (optionType == 2) {
                // if_name
                for (int j = 0; j < optionLen; j++) {
                    this.interfaceName.append((char) bb.get(offset + j));
                }
                val = this.interfaceName;
            } else if (optionType == 9) {
                // if_tsresol (assume power of 10)
                val = this.timestampResolution = bb.get(offset);
                this.timestampConversionToNanos = (long) Math.pow(10, (9 - this.timestampResolution));
            } else if (optionType == 0 && optionLen == 0) {
                // end of options
                LOG.trace("onOption: end");
            }
            if (LOG.isDebugEnabled()) {
                LOG.debug("onOption: type={}, len={}, val=[{}], offset={}", optionType, optionLen, val, offset);
            }
        }


        public int getLinkType() {
            return linkType;
        }

        public long getSnapLen() {
            return snapLen;
        }

        public StringBuilder getInterfaceName() {
            return interfaceName;
        }

        public byte getTimestampResolution() {
            return timestampResolution;
        }

        public long getTimestampConversionToNanos() {
            return timestampConversionToNanos;
        }

        @Override
        public String toString() {
            return "InterfaceBlock{" +
                    "byteBuffer=" + byteBuffer +
                    ", offset=" + offset +
                    ", type=" + type +
                    ", length=" + length +
                    ", linkType=" + linkType +
                    ", snapLen=" + snapLen +
                    ", interfaceName=" + interfaceName +
                    ", timestampResolution=" + timestampResolution +
                    ", timestampConversionToNanos=" + timestampConversionToNanos +
                    '}';
        }
    }

    public static class EnhancedPacketBlock extends BaseBlock {
        public static final int TYPE = 0x00000006;

        private long interfaceID;
        private long timestamp;
        private long capturedPacketLength;
        private long originalPacketLength;
        private int payloadStartOffset;

        @Override
        public Block init(ByteBuffer bb, int type, int offset) {
            super.init(bb, type, offset);

            this.interfaceID = ByteUtils.getUInt(bb, offset + 8);

            // needs to be combined with interface blocks timestamp resolution (e.g. currentTimestampMultiplier)
            int timestampHigh = bb.getInt(offset + 12);
            int timestampLow = bb.getInt(offset + 16);
            this.timestamp = ((long) timestampHigh << 32) + timestampLow;
            this.capturedPacketLength = ByteUtils.getUInt(bb, offset + 20);
            this.originalPacketLength = ByteUtils.getUInt(bb, offset + 24);
            this.payloadStartOffset = offset + 28;

            return this;
        }

        public long getInterfaceID() {
            return interfaceID;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public long getCapturedPacketLength() {
            return capturedPacketLength;
        }

        public long getOriginalPacketLength() {
            return originalPacketLength;
        }

        public int getPayloadStartOffset() {
            return payloadStartOffset;
        }

        public long getCaptureTimestamp(PcapNG pcapNG) {
            return this.timestamp * pcapNG.getCurrentTimestampMultiplier();
        }

        @Override
        public String toString() {
            return "EnhancedPacketBlock{" +
                    "length=" + length +
                    ", interfaceID=" + interfaceID +
                    ", timestamp=" + timestamp +
                    ", capturedPacketLength=" + capturedPacketLength +
                    ", originalPacketLength=" + originalPacketLength +
                    ", payloadStartOffset=" + payloadStartOffset +
                    '}';
        }

    }

    public interface PayloadParser {
        boolean parse(PcapNG pcapNG, EnhancedPacketBlock enhancedPacketBlock);
    }

    public static class EthernetIPV4UdpPayloadParser implements PayloadParser {
        private long captureTimestamp;

        private int srcIP;
        private int destIP;

        private int srcPort;
        private int destPort;
        private ByteBuffer buffer;

        @Override
        public boolean parse(PcapNG pcapNG, EnhancedPacketBlock enhancedPacketBlock) {
            boolean success = false;
            this.captureTimestamp = enhancedPacketBlock.getCaptureTimestamp(pcapNG);

            if (pcapNG.getCurrentLinkType() == 1) {
                this.buffer = enhancedPacketBlock.getByteBuffer();
                this.buffer.limit(this.buffer.capacity());
                this.buffer.order(ByteOrder.BIG_ENDIAN);

                int framePos = enhancedPacketBlock.getPayloadStartOffset();
                int ethernetType = buffer.getShort(framePos + 12);
                if (ethernetType == 0x0800) {
                    // IPV4
                    framePos += 14;
                    byte b0 = buffer.get(framePos);
                    byte proto = buffer.get(framePos + 9);
                    this.srcIP = buffer.getInt(framePos + 12);
                    this.destIP = buffer.getInt(framePos + 16);
                    int version = b0 >> 4;
                    int ihl = b0 & 0x0000000F;
                    int totalLen = buffer.getShort(framePos + 2);

                    if (LOG.isTraceEnabled()) {
                        LOG.trace("\tversion={}, ihl={}({}), totalLen={}, proto={}, srcIP={}, destIP={}",
                                version, ihl, ihl * 4, totalLen, proto, srcIP, destIP);
                    }

                    if (proto == 0x11) {
                        // UDP
                        framePos += ihl * 4;
                        this.srcPort = ByteUtils.getUShort(buffer, framePos);
                        this.destPort = ByteUtils.getUShort(buffer, framePos + 2);
                        int len = ByteUtils.getUShort(buffer, framePos + 4);
                        if (LOG.isTraceEnabled()) {
                            LOG.trace("\t\tsrcPort={}, destPort={}, len={}", srcPort, destPort, len);
                        }

                        // UDP data
                        framePos += 8;
                        int payloadLen = len - 8;
                        buffer.limit(framePos + payloadLen).position(framePos);

                        success = true;
                    }
                }
            }
            return success;
        }

        public long getCaptureTimestamp() {
            return captureTimestamp;
        }

        public int getSrcIP() {
            return srcIP;
        }

        public int getDestIP() {
            return destIP;
        }

        public int getSrcPort() {
            return srcPort;
        }

        public int getDestPort() {
            return destPort;
        }

        public ByteBuffer getBuffer() {
            return buffer;
        }
    }

    public static void main(String[] args) {
        RateSensor sensor = new RateSensor(2_000_000);

        byte[] tp_buffer = new byte[2048];
        TPPacketProcessor processor = new TPPacketProcessor();
        processor.start("mapped");

        File file = new File("C:/dev/data/pcapng/data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap");
        EthernetIPV4UdpPayloadParser parser = new EthernetIPV4UdpPayloadParser();
        try (PcapNG pcapNG = new PcapNG(file)) {
            for (Block block : pcapNG) {
                if (block.getType() == EnhancedPacketBlock.TYPE) {
                    EnhancedPacketBlock enhancedPacketBlock = (EnhancedPacketBlock) block;

                    if (parser.parse(pcapNG, enhancedPacketBlock)) {
                        processor.process(enhancedPacketBlock.getByteBuffer());
                    }
                }

                if (sensor.addObservations(1)) {
                    LOG.info(String.format("[%,10d] %s (%.0f bps)", sensor.getCount(), block, sensor.getRate()));
                }
            }
        } catch (IOException e) {
            LOG.error("Error opening pcap file", e);
        }

        processor.done();
        LOG.info(String.format("Done - %,10d", sensor.getCount()));

    }
}
