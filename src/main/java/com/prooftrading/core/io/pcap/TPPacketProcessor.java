package com.prooftrading.core.io.pcap;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;
import com.prooftrading.core.data.propfeed.iex.tops.TOPSMessageFactory;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntRBTreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.function.Consumer;

@SuppressWarnings({"WeakerAccess", "unused"})
public class TPPacketProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(TPPacketProcessor.class);

    private static final Consumer<PropFeedMessage> NOOP_CONSUMER = propFeedMessage -> {};

    private final TPPacket tpPacket = new TPPacket();
    private final Consumer<PropFeedMessage> messageConsumer;
    private final Object2IntMap<String> messageCounter = new Object2IntRBTreeMap<>();
    private final TOPSMessageFactory topsMessageFactory = new TOPSMessageFactory();

    private long packets = 0;
    private long messages= 0;
    private long bytes = 0;
    private long startTime = 0;

    public TPPacketProcessor() {
        this(NOOP_CONSUMER);
    }

    public TPPacketProcessor(Consumer<PropFeedMessage> messageConsumer) {
        this.messageConsumer = messageConsumer;
    }

    public void start(String desc) {
        LOG.info(desc);
        this.startTime = System.currentTimeMillis();
    }

    public void process(ByteBuffer bb) {
        int len = bb.remaining();
        this.tpPacket.set(bb);
        this.bytes += len;

        for(TPPacket.TPMessage msg : this.tpPacket) {
            this.messages++;
            PropFeedMessage feedMessage = topsMessageFactory.fromTPMessage(msg);
            String className = feedMessage.getClass().getSimpleName();
            int n = messageCounter.getInt(className);
            messageCounter.put(className, n + 1);

            this.messageConsumer.accept(feedMessage);
        }
        this.packets++;
    }

    public void done() {
        double durationSecs = (System.currentTimeMillis() - this.startTime)/1000.0;

        LOG.info(String.format(
                "Done - packets=[%,d], messages=[%,d], bytes=[%,d]   (%,.0f pps | %,.0f mps)",
                packets, messages, bytes, packets / durationSecs, messages / durationSecs));
        for(Object2IntMap.Entry<String> entry: messageCounter.object2IntEntrySet()){
            LOG.info(String.format("%35s - %,10d", entry.getKey(), entry.getIntValue()));
        }
    }

    public long getPackets() {
        return packets;
    }

    public long getMessages() {
        return messages;
    }

    public long getBytes() {
        return bytes;
    }

    public Object2IntMap<String> getMessageCounter() {
        return messageCounter;
    }

    public static void check(TPPacketProcessor proc1, TPPacketProcessor proc2) {
        LOG.info("[{}] packets", proc1.getPackets() == proc2.getPackets() ? "pass" : "FAIL");
        LOG.info("[{}] messages", proc1.getMessages() == proc2.getMessages() ? "pass" : "FAIL");
        LOG.info("[{}] bytes", proc1.getBytes() == proc2.getBytes() ? "pass" : "FAIL");
    }
}
