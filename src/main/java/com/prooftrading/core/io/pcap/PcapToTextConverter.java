package com.prooftrading.core.io.pcap;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * An example class to use the PcapNG parser to convert the binary files to text format
 */
public class PcapToTextConverter {
    private static final Logger LOG = LoggerFactory.getLogger(PcapToTextConverter.class);

    public static void main(String[] args) throws Exception {
        // parse and validate args
        if (args.length < 2) {
            LOG.error("{}{}", System.lineSeparator(), getUsage());
            return;
        }
        File filePcap = new File(args[0]);
        if (!filePcap.exists()) {
            throw new IllegalArgumentException("PCAP file doesn't exist: " + filePcap);
        }
        File outputDir = new File(args[1]);
        if (!outputDir.exists()) {
            LOG.info("Creating output directory: {}", outputDir.getAbsolutePath());
            if (!outputDir.mkdirs()){
                throw new IllegalStateException("Could not create output dir: " + outputDir);
            }
        }
        boolean csv = false;
        if (args.length > 2) {
            csv = "csv".equalsIgnoreCase(args[2]);
        }

        // do work
        LOG.info("Converting Pcap File: inputFile={}, outputDir={}, csv={}", filePcap, outputDir.getAbsolutePath(), csv);
        try(FilePerTypeConsumer messageConsumer  = new FilePerTypeConsumer(outputDir, csv)) {
            processFile(filePcap, 1_000_000, messageConsumer);
        }
    }

    public static String getUsage(){
        return String.join(
                System.lineSeparator(),
                new String[] {
                        "",
                        "USAGE: java " + PcapToTextConverter.class.getName() + " PCAP_FILE  OUTPUT_DIR  [CSV]",
                        "Convert the IEX TOPS binary file to text format, creating a file per message type",
                        "",
                        "Example: java  -cp proof-iex-parser-all.jar " + PcapToTextConverter.class.getName() + "  C:/dev/data/pcapng/data_feeds_20190610_20190610_IEXTP1_TOPS1.6.pcap  output",
                        ""
                });
    }

    private static void processFile(File filePcap, int rateSensorInterval, Consumer<PropFeedMessage> messageConsumer) {
        RateSensor sensor = new RateSensor(rateSensorInterval);

        TPPacketProcessor processor = new TPPacketProcessor(messageConsumer);
        processor.start("mapped");
        PcapNG.EthernetIPV4UdpPayloadParser parser = new PcapNG.EthernetIPV4UdpPayloadParser();
        try (PcapNG pcapNG = new PcapNG(filePcap)) {
            for (PcapNG.Block block : pcapNG) {
                if (block.getType() == PcapNG.EnhancedPacketBlock.TYPE) {
                    PcapNG.EnhancedPacketBlock enhancedPacketBlock = (PcapNG.EnhancedPacketBlock) block;

                    if (parser.parse(pcapNG, enhancedPacketBlock)) {
                        processor.process(enhancedPacketBlock.getByteBuffer());
                    }
                }

                if (sensor.addObservations(1)) {
                    LOG.info(String.format("[%,10d] %s (%.0f bps)", sensor.getCount(), block, sensor.getRate()));
                }
            }
        } catch (IOException e) {
            LOG.error("Error opening pcap file", e);
        }

        processor.done();
        LOG.info(String.format("Done - %,10d", sensor.getCount()));
    }

    /**
     * Callback for the TPPacketProcessor that will write a file per type, line by line
     */
    private static class FilePerTypeConsumer implements Consumer<PropFeedMessage>, AutoCloseable {
        private final File outputDir;
        private final boolean csv;
        private final Map<String, BufferedWriter> fileWriters = new HashMap<>();
        private final Map<String, CSVHelper> csvHelpers = new HashMap<>();

        private FilePerTypeConsumer(File outputDir, boolean csv) {
            this.outputDir = outputDir;
            this.csv = csv;
        }

        @Override
        public void accept(PropFeedMessage propFeedMessage) {
            // use message name (e.g. QuoteUpdateMessage) as key
            String messageName = propFeedMessage.getClass().getSimpleName();

            // get/create file writer per message type (ascii, overwrite old files)
            BufferedWriter writer = this.fileWriters.get(messageName);
            if (writer == null) {
                File outputFile = new File(outputDir, messageName + "." + (csv ? "csv" : "txt"));
                try {
                    writer = new BufferedWriter(new FileWriter(outputFile, StandardCharsets.US_ASCII, false));
                    this.fileWriters.put(messageName, writer);
                } catch (IOException e) {
                    throw new IllegalStateException("Could not create output file for message: " + messageName + ", file: " + outputFile, e);
                }
            }

            // write
            try {
                if (this.csv) {
                    CSVHelper helper = this.csvHelpers.get(messageName);
                    if (helper == null) {
                        helper = new CSVHelper(propFeedMessage.getClass());
                        writer.write(helper.getHeader());
                        writer.newLine();
                        this.csvHelpers.put(messageName, helper);
                    }

                    writer.write(helper.toRecord(propFeedMessage));
                } else {
                    writer.write(propFeedMessage.toString());
                }
                writer.newLine();
            } catch (IOException e) {
                throw new IllegalStateException("Could not write message to file.  message: " + propFeedMessage, e);
            }

        }

        @Override
        public void close() throws Exception {
            // flush data to files
            for(BufferedWriter bw : this.fileWriters.values()) {
                bw.close();
            }
        }
    }

}
