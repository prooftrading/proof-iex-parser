package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class OfficialPriceMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'X';

    public static final char PRICETYPE_OPENING_PRICE = 'Q';
    public static final char PRICETYPE_CLOSING_PRICE = 'M';

    private char priceType = ' ';
    private long timestamp = 0L;
    private StringBuilder symbol = new StringBuilder();
    private long officialPrice = 0L;

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.priceType = msg.getChar(1);
        this.timestamp = msg.getLong(2);
        this.symbol = msg.getString(10, 8, this.symbol);
        this.officialPrice = msg.getLong(18);
        return this;
    }

    public char getPriceType() {
        return this.priceType;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public StringBuilder getSymbol() {
        return this.symbol;
    }

    public long getOfficialPrice() {
        return this.officialPrice;
    }

    @Override
    public String toString() {
        return "OfficialPriceMessage{" +
                "priceType=" + priceType +
                ", timestamp=" + timestamp +
                ", symbol='" + symbol + "'" +
                ", officialPrice=" + officialPrice +
                '}';
    }
}