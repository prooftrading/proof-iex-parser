package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class AuctionInfoMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'A';

    public static final char AUCTIONTYPE_OPENING = 'O';
    public static final char AUCTIONTYPE_CLOSING = 'C';
    public static final char AUCTIONTYPE_IPO = 'I';
    public static final char AUCTIONTYPE_HALT = 'H';
    public static final char AUCTIONTYPE_VOLATILITY = 'V';

    public static final byte IMBALANCESIDE_BUY_SIDE_IMBALANCE = 'B';
    public static final byte IMBALANCESIDE_SELL_SIDE_IMBALANCE = 'S';
    public static final byte IMBALANCESIDE_NO_IMBALANCE = 'N';

    private char auctionType = ' ';
    private long timestamp = 0L;
    private StringBuilder symbol = new StringBuilder();
    private int pairedShares = 0;
    private long referencePrice = 0L;
    private long indicativeClearingPrice = 0L;
    private int imbalanceShares = 0;
    private byte imbalanceSide = 0;
    private byte extensionNumber = 0;
    private int scheduledAuctionTime = 0;
    private long auctionBookClearingPrice = 0;
    private long collarReferencePrice = 0;
    private long lowerAuctionCollar = 0;
    private long upperAuctionCollar = 0;

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.auctionType = msg.getChar(1);
        this.timestamp = msg.getLong(2);
        this.symbol = msg.getString(10, 8, this.symbol);
        this.pairedShares = msg.getInt(18);
        this.referencePrice = msg.getLong(22);
        this.indicativeClearingPrice = msg.getLong(30);
        this.imbalanceShares = msg.getInt(38);
        this.imbalanceSide = msg.getByte(42);
        this.extensionNumber = msg.getByte(43);
        this.scheduledAuctionTime = msg.getInt(44);
        this.auctionBookClearingPrice = msg.getLong(48);
        this.collarReferencePrice = msg.getLong(56);
        this.lowerAuctionCollar = msg.getLong(64);
        this.upperAuctionCollar = msg.getLong(72);
        return this;
    }

    public char getAuctionType() {
        return this.auctionType;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public StringBuilder getSymbol() {
        return this.symbol;
    }

    public int getPairedShares() {
        return this.pairedShares;
    }

    public long getReferencePrice() {
        return this.referencePrice;
    }

    public long getIndicativeClearingPrice() {
        return this.indicativeClearingPrice;
    }

    public int getImbalanceShares() {
        return this.imbalanceShares;
    }

    public byte getImbalanceSide() {
        return this.imbalanceSide;
    }

    public byte getExtensionNumber() {
        return this.extensionNumber;
    }

    public int getScheduledAuctionTime() {
        return this.scheduledAuctionTime;
    }

    public long getAuctionBookClearingPrice() {
        return this.auctionBookClearingPrice;
    }

    public long getCollarReferencePrice() {
        return this.collarReferencePrice;
    }

    public long getLowerAuctionCollar() {
        return this.lowerAuctionCollar;
    }

    public long getUpperAuctionCollar() {
        return this.upperAuctionCollar;
    }

    @Override
    public String toString() {
        return "AuctionInfoMessage{" +
                "auctionType=" + auctionType +
                ", timestamp=" + timestamp +
                ", symbol='" + symbol + "'" +
                ", pairedShares=" + pairedShares +
                ", referencePrice=" + referencePrice +
                ", indicativeClearingPrice=" + indicativeClearingPrice +
                ", imbalanceShares=" + imbalanceShares +
                ", imbalanceSide=" + imbalanceSide +
                ", extensionNumber=" + extensionNumber +
                ", scheduledAuctionTime=" + scheduledAuctionTime +
                ", auctionBookClearingPrice=" + auctionBookClearingPrice +
                ", collarReferencePrice=" + collarReferencePrice +
                ", lowerAuctionCollar=" + lowerAuctionCollar +
                ", upperAuctionCollar=" + upperAuctionCollar +
                '}';
    }
}