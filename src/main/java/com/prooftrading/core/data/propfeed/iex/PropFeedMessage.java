package com.prooftrading.core.data.propfeed.iex;

public interface PropFeedMessage {

    char getMessageType();
    PropFeedMessage init(TPPacket.TPMessage msg);

    interface PropFeedMessageFactory {
        PropFeedMessage fromTPMessage(TPPacket.TPMessage msg);

        static char getMessageType(TPPacket.TPMessage msg) {
            return msg.getChar(0);
        }
    }

    abstract class AbstractPropFeedMessage implements PropFeedMessage {
        private char messageType;

        @Override
        public char getMessageType() {
            return messageType;
        }

        @Override
        public PropFeedMessage init(TPPacket.TPMessage msg) {
            this.messageType = PropFeedMessageFactory.getMessageType(msg);
            return this;
        }
    }
}

