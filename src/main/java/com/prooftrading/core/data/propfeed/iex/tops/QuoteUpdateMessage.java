package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class QuoteUpdateMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'Q';

    public static final int FLAGS_HALTED_PAUSED_UNAVAILABLE = 0x80;
    public static final int FLAGS_PRE_POST_MARKET_SESSION = 0x40;

    private byte flags = 0;
    private long timestamp = 0L;
    private StringBuilder symbol = new StringBuilder();
    private int bidSize = 0;
    private long bidPrice = 0L;
    private long askPrice = 0L;
    private int askSize = 0;

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.flags = msg.getByte(1);
        this.timestamp = msg.getLong(2);
        this.symbol = msg.getString(10, 8, this.symbol);
        this.bidSize = msg.getInt(18);
        this.bidPrice = msg.getLong(22);
        this.askPrice = msg.getLong(30);
        this.askSize = msg.getInt(38);
        return this;
    }

    public byte getFlags() {
        return this.flags;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public StringBuilder getSymbol() {
        return this.symbol;
    }

    public int getBidSize() {
        return this.bidSize;
    }

    public long getBidPrice() {
        return this.bidPrice;
    }

    public long getAskPrice() {
        return this.askPrice;
    }

    public int getAskSize() {
        return this.askSize;
    }

    @Override
    public String toString() {
        return "QuoteUpdateMessage{" +
                "flags=" + flags +
                ", timestamp=" + timestamp +
                ", symbol='" + symbol + "'" +
                ", bidSize=" + bidSize +
                ", bidPrice=" + bidPrice +
                ", askPrice=" + askPrice +
                ", askSize=" + askSize +
                '}';
    }
}