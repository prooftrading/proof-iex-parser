package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class ShortSalePriceTestStatusMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'P';

    public static final char DETAIL_NO_PRICE_TEST = ' ';
    public static final char DETAIL_ACTIVATED = 'A';
    public static final char DETAIL_CONTINUED = 'C';
    public static final char DETAIL_DEACTIVATED = 'D';
    public static final char DETAIL_NOT_AVAILABLE = 'N';

    private byte shortSalePriceTestStatus = 0;
    private long timestamp = 0L;
    private StringBuilder symbol = new StringBuilder();
    private char detail = ' ';

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.shortSalePriceTestStatus = msg.getByte(1);
        this.timestamp = msg.getLong(2);
        this.symbol = msg.getString(10, 8, this.symbol);
        this.detail = msg.getChar(18);
        return this;
    }

    public byte getShortSalePriceTestStatus() {
        return this.shortSalePriceTestStatus;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public StringBuilder getSymbol() {
        return this.symbol;
    }

    public char getDetail() {
        return this.detail;
    }

    @Override
    public String toString() {
        return "ShortSalePriceTestStatusMessage{" +
                "shortSalePriceTestStatus=" + shortSalePriceTestStatus +
                ", timestamp=" + timestamp +
                ", symbol='" + symbol + "'" +
                ", detail=" + detail +
                '}';
    }
}