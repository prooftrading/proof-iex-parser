package com.prooftrading.core.data.propfeed.iex.tops;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;


/**
* Generated Message Factory Class
*/
public class TOPSMessageFactory implements PropFeedMessage.PropFeedMessageFactory {

    private static final Logger LOG = LoggerFactory.getLogger(TOPSMessageFactory.class);

    private final SystemEventMessage systemEventMessage = new SystemEventMessage();
    private final SecurityDirectoryMessage securityDirectoryMessage = new SecurityDirectoryMessage();
    private final TradingStatusMessage tradingStatusMessage = new TradingStatusMessage();
    private final OperationalHaltStatusMessage operationalHaltStatusMessage = new OperationalHaltStatusMessage();
    private final ShortSalePriceTestStatusMessage shortSalePriceTestStatusMessage = new ShortSalePriceTestStatusMessage();
    private final QuoteUpdateMessage quoteUpdateMessage = new QuoteUpdateMessage();
    private final TradeReportMessage tradeReportMessage = new TradeReportMessage();
    private final OfficialPriceMessage officialPriceMessage = new OfficialPriceMessage();
    private final TradeBreakMessage tradeBreakMessage = new TradeBreakMessage();
    private final AuctionInfoMessage auctionInfoMessage = new AuctionInfoMessage();

    @Override
    public PropFeedMessage fromTPMessage(TPPacket.TPMessage msg) {
        try {
            char msgType = PropFeedMessage.PropFeedMessageFactory.getMessageType(msg);
            switch (msgType) {
                case 'S':   return  systemEventMessage.init(msg);
                case 'D':   return  securityDirectoryMessage.init(msg);
                case 'H':   return  tradingStatusMessage.init(msg);
                case 'O':   return  operationalHaltStatusMessage.init(msg);
                case 'P':   return  shortSalePriceTestStatusMessage.init(msg);
                case 'Q':   return  quoteUpdateMessage.init(msg);
                case 'T':   return  tradeReportMessage.init(msg);
                case 'X':   return  officialPriceMessage.init(msg);
                case 'B':   return  tradeBreakMessage.init(msg);
                case 'A':   return  auctionInfoMessage.init(msg);
                default:
                    LOG.warn("Unknown msgType={}, msg={}", msgType, msg);
                    break;
            }
        } catch (Exception ex) {
            LOG.error("Error parsing msg: " + msg, ex);
        }
        return null;
    }

}