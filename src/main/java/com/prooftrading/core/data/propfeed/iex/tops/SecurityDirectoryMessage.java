package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class SecurityDirectoryMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'D';

    public static final int FLAGS_TEST_SECURITY_MASK = 0x80;
    public static final int FLAGS_WHEN_ISSUED_MASK = 0x40;
    public static final int FLAGS_ETP_MASK = 0x20;

    private byte flags = 0;
    private long timestamp = 0L;
    private StringBuilder symbol = new StringBuilder();
    private int roundLotSize = 0;
    private long adjustedPOCPrice = 0L;
    private byte luldTier = 0;

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.flags = msg.getByte(1);
        this.timestamp = msg.getLong(2);
        this.symbol = msg.getString(10, 8, this.symbol);
        this.roundLotSize = msg.getInt(18);
        this.adjustedPOCPrice = msg.getLong(22);
        this.luldTier = msg.getByte(30);
        return this;
    }

    public byte getFlags() {
        return this.flags;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public StringBuilder getSymbol() {
        return this.symbol;
    }

    public int getRoundLotSize() {
        return this.roundLotSize;
    }

    public long getAdjustedPOCPrice() {
        return this.adjustedPOCPrice;
    }

    public byte getLuldTier() {
        return this.luldTier;
    }

    @Override
    public String toString() {
        return "SecurityDirectoryMessage{" +
                "flags=" + flags +
                ", timestamp=" + timestamp +
                ", symbol='" + symbol + "'" +
                ", roundLotSize=" + roundLotSize +
                ", adjustedPOCPrice=" + adjustedPOCPrice +
                ", luldTier=" + luldTier +
                '}';
    }
}