package com.prooftrading.core.data.propfeed.iex;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;

public class TPPacket implements TPHeader, Iterable<TPPacket.TPMessage> {
    public static final int MAX_PACKET_SIZE = 1500;

    private final ByteBuffer buffer = ByteBuffer.allocate(MAX_PACKET_SIZE).order(ByteOrder.LITTLE_ENDIAN);
    private final MessageIterator messageIterator = new MessageIterator(this.buffer);

    private byte version;
    private short protocol;
    private int channelId;
    private int sessionId;
    private short payloadLen;
    private short messageCount;
    private long streamOffset;
    private long firstSequence;
    private long sendTime;

    public void set(byte[] data) {
        set(data, 0, data.length);
    }

    public void set(byte[] data, int offset, int len) {
        this.buffer.clear();
        this.buffer.put(data, offset, len);
        this.buffer.flip();
        this.version = this.buffer.get(0);
        this.protocol =  this.buffer.getShort(2);
        this.channelId =  this.buffer.getInt(4);
        this.sessionId =  this.buffer.getInt(8);
        this.payloadLen =  this.buffer.getShort(12);
        this.messageCount =  this.buffer.getShort(14);
        this.streamOffset =  this.buffer.getLong(16);
        this.firstSequence =  this.buffer.getLong(24);
        this.sendTime =  this.buffer.getLong(32);

        this.messageIterator.reset(this, getMessageCount());
    }

    public void set(ByteBuffer bb) {
        this.buffer.clear();
        this.buffer.put(bb);
        this.buffer.flip();
        this.version = this.buffer.get(0);
        this.protocol =  this.buffer.getShort(2);
        this.channelId =  this.buffer.getInt(4);
        this.sessionId =  this.buffer.getInt(8);
        this.payloadLen =  this.buffer.getShort(12);
        this.messageCount =  this.buffer.getShort(14);
        this.streamOffset =  this.buffer.getLong(16);
        this.firstSequence =  this.buffer.getLong(24);
        this.sendTime =  this.buffer.getLong(32);

        this.messageIterator.reset(this, getMessageCount());
    }

    public byte getVersion() {
        return version;
    }

    public short getProtocol() {
        return protocol;
    }

    public int getChannelId() {
        return channelId;
    }

    public int getSessionId() {
        return sessionId;
    }

    public short getPayloadLen() {
        return payloadLen;
    }

    public short getMessageCount() {
        return messageCount;
    }

    public long getStreamOffset() {
        return streamOffset;
    }

    public long getFirstSequence() {
        return firstSequence;
    }

    public long getSendTime() {
        return sendTime;
    }

    @Override
    public Iterator<TPMessage> iterator() {
        return this.messageIterator;
    }

    @Override
    public String toString() {
        return "TPPacket{" +
                "buffer=" + buffer +
                ", version=" + version +
                ", protocol=" + protocol +
                ", channelId=" + channelId +
                ", sessionId=" + sessionId +
                ", payloadLen=" + payloadLen +
                ", messageCount=" + messageCount +
                ", streamOffset=" + streamOffset +
                ", firstSequence=" + firstSequence +
                ", sendTime=" + sendTime +
                '}';
    }

    public static class TPMessage {
        private ByteBuffer bufferPacket;
        private int dataOffset;
        private int limit;
        private TPHeader header;
        private long sequence;
        private int msgIndex;

        private TPMessage(ByteBuffer bufferPacket){
            this.bufferPacket = bufferPacket;
        }

        public TPMessage reset(TPHeader header, long sequence, int msgIndex){
            this.dataOffset = this.getData().position();
            this.limit = this.getData().limit();

            this.header = header;
            this.sequence = sequence;
            this.msgIndex = msgIndex;
            return this;
        }

        public ByteBuffer getData(){
            return this.bufferPacket;
        }

        public int getDataOffset() {
            return dataOffset;
        }

        public int getLimit() {
            return limit;
        }

        public TPHeader getHeader() {
            return this.header;
        }

        public long getSequence() {
            return sequence;
        }

        public int getMsgIndex() {
            return msgIndex;
        }

        public int length(){
            return this.limit - this.dataOffset;
        }

        public char getChar(int offset) {
            return (char) this.bufferPacket.get(this.dataOffset + offset);
        }

        public byte getByte(int offset) {
            return this.bufferPacket.get(this.dataOffset + offset);
        }

        public short getShort(int offset) {
            return this.bufferPacket.getShort(this.dataOffset + offset);
        }

        public int getInt(int offset) {
            return this.bufferPacket.getInt(this.dataOffset + offset);
        }

        public long getLong(int offset) {
            return this.bufferPacket.getLong(this.dataOffset + offset);
        }

        /**
         * Read chars from data buffer.
         * Excludes spaces since symbols are space padded
         * @param offset
         * @param len
         * @param dest
         * @return
         */
        public StringBuilder getString(int offset, int len, StringBuilder dest) {
            dest.setLength(0);
            for(int i = 0; i < len; i++) {
                char c = (char) this.bufferPacket.get(this.dataOffset + offset + i);
                if (c != ' ') {
                    dest.append(c);
                }
            }
            return dest;
        }

        @Override
        public String toString() {
            return "TPMessage{" +
                    "bufferPacket=" + bufferPacket +
                    ", index=" + dataOffset +
                    ", limit=" + limit +
                    ", sequence=" + sequence +
                    ", msgIndex=" + msgIndex +
                    '}';
        }
    }

    static class MessageIterator implements Iterator<TPMessage> {

        private final ByteBuffer bufferPacket;
        private final TPMessage message;

        private TPHeader header;
        private int messageCount;
        private int origLimit;
        private int index;
        private int messagesRead;

        public MessageIterator(ByteBuffer bufferPacket) {
            this.bufferPacket = bufferPacket;
            this.message = new TPMessage(this.bufferPacket);
        }

        public void reset(TPHeader header, int messageCount){
            this.header = header;
            this.messageCount = messageCount;
            this.origLimit = this.bufferPacket.limit();
            this.index = 40;
            this.messagesRead = 0;
        }

        @Override
        public boolean hasNext() {
            return messageCount > 0 && messagesRead < messageCount;
        }

        @Override
        public TPMessage next() {
            if (!hasNext()) {
                return null;
            }

            this.bufferPacket.limit(this.origLimit);
            int len = this.bufferPacket.getShort(this.index);
            // LOG.info("index={}, len={}", this.index, len);
            long sequence = header.getFirstSequence() + messagesRead;
            int msgIndex = messagesRead;

            this.index += 2;
            this.bufferPacket.position(this.index);
            this.bufferPacket.limit(this.index + len);
            this.index += len;
            this.messagesRead++;
            return this.message.reset(header, sequence, msgIndex);
        }
    }

}
