package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class OperationalHaltStatusMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'O';

    public static final char OPERATIONALHALTSTATUS_OP_TRADING_HALTED = 'O';
    public static final char OPERATIONALHALTSTATUS_NOT_OP_HALTED = 'N';

    private char operationalHaltStatus = ' ';
    private long timestamp = 0L;
    private StringBuilder symbol = new StringBuilder();

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.operationalHaltStatus = msg.getChar(1);
        this.timestamp = msg.getLong(2);
        this.symbol = msg.getString(10, 8, this.symbol);
        return this;
    }

    public char getOperationalHaltStatus() {
        return this.operationalHaltStatus;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public StringBuilder getSymbol() {
        return this.symbol;
    }

    @Override
    public String toString() {
        return "OperationalHaltStatusMessage{" +
                "operationalHaltStatus=" + operationalHaltStatus +
                ", timestamp=" + timestamp +
                ", symbol='" + symbol + "'" +
                '}';
    }
}