package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class TradeBreakMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'B';

    public static final int SALECONDITIONFLAGS_ISO = 0x80;
    public static final int SALECONDITIONFLAGS_EXTENDING_HOURS = 0x40;
    public static final int SALECONDITIONFLAGS_ODD_LOT = 0x20;
    public static final int SALECONDITIONFLAGS_TRADE_THROUGH_EXEMPT = 0x10;
    public static final int SALECONDITIONFLAGS_CROSS = 0x08;

    private byte saleConditionFlags = 0;
    private long timestamp = 0L;
    private StringBuilder symbol = new StringBuilder();
    private int size = 0;
    private long price = 0L;
    private long tradeID = 0L;

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.saleConditionFlags = msg.getByte(1);
        this.timestamp = msg.getLong(2);
        this.symbol = msg.getString(10, 8, this.symbol);
        this.size = msg.getInt(18);
        this.price = msg.getLong(22);
        this.tradeID = msg.getLong(30);
        return this;
    }

    public byte getSaleConditionFlags() {
        return this.saleConditionFlags;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public StringBuilder getSymbol() {
        return this.symbol;
    }

    public int getSize() {
        return this.size;
    }

    public long getPrice() {
        return this.price;
    }

    public long getTradeID() {
        return this.tradeID;
    }

    @Override
    public String toString() {
        return "TradeBreakMessage{" +
                "saleConditionFlags=" + saleConditionFlags +
                ", timestamp=" + timestamp +
                ", symbol='" + symbol + "'" +
                ", size=" + size +
                ", price=" + price +
                ", tradeID=" + tradeID +
                '}';
    }
}