package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class SystemEventMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'S';

    public static final char SYSTEMEVENT_STARTOFMESSAGES = 'O';
    public static final char SYSTEMEVENT_STARTOFSYSTEMHOURS = 'S';
    public static final char SYSTEMEVENT_STARTOFREGULARMARKETHOURS = 'R';
    public static final char SYSTEMEVENT_ENDOFREGULARMARKETHOURS = 'M';
    public static final char SYSTEMEVENT_ENDOFSYSTEMHOURS = 'E';
    public static final char SYSTEMEVENT_ENDOFMESSAGES = 'C';

    private char systemEvent = ' ';
    private long timestamp = 0L;

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.systemEvent = msg.getChar(1);
        this.timestamp = msg.getLong(2);
        return this;
    }

    public char getSystemEvent() {
        return this.systemEvent;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    @Override
    public String toString() {
        return "SystemEventMessage{" +
                "systemEvent=" + systemEvent +
                ", timestamp=" + timestamp +
                '}';
    }
}