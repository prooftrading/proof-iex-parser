package com.prooftrading.core.data.propfeed.iex;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

public class PropFeedMessageGenerator {

    private static final Logger LOG = LoggerFactory.getLogger(PropFeedMessageGenerator.class);

    private String schemaResourcePath;
    private Template templateMessage;
    private Template templateFactory;

    public PropFeedMessageGenerator(String schemaResourcePath, String messageTemplatePath, String factoryTemplatePath)
            throws IOException {
        this.schemaResourcePath = schemaResourcePath;
        Configuration cfg = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        this.templateMessage = cfg.getTemplate(messageTemplatePath);
        this.templateFactory = cfg.getTemplate(factoryTemplatePath);
    }

    public void generate(String packageName, File destDir, List<String> messageImports, List<String> factoryImports)
            throws IOException, TemplateException {
        // load message spec from json
        Map<String, Object> data = this.readDataFromJson();

        // prepare target location
        if (!destDir.exists()) {
            LOG.info("Creating destination dir: {}", destDir);
            if (!destDir.mkdirs()) {
                throw new IllegalStateException("Could not create destination dir: " + destDir);
            }
        }

        // generate each message
        @SuppressWarnings("unchecked") List<Map<String, Object>> messages = (List<Map<String, Object>>) data.get("messages");
        for (Map<String, Object> message : messages) {
            // create message state
            Map<String, Object> messageState = new HashMap<>();
            messageState.put("package", packageName);
            messageState.put("imports", messageImports);
            messageState.putAll(message);

            File fileMessage = new File(destDir, messageState.get("className") + ".java");
            processTemplateToFile(fileMessage, this.templateMessage, messageState);
        }

        // create factory state
        Map<String, Object> factoryState = new HashMap<>();
        factoryState.put("package", packageName);
        factoryState.put("imports", factoryImports);
        factoryState.putAll(data);

        // generate factory class
        File fileFactory = new File(destDir, factoryState.get("factoryClassName") + ".java");
        processTemplateToFile(fileFactory, this.templateFactory, factoryState);
    }

    private void processTemplateToFile(File file, Template template, Map<String, Object> messageState) throws IOException, TemplateException {
        try (FileWriter fw = new FileWriter(file)) {
            template.process(messageState, fw);
        }
        if (LOG.isTraceEnabled()) {
            LOG.trace("\n{}\n", new String(Files.readAllBytes(file.toPath())));
        }
        LOG.info("Processed template to [{}] ({} bytes)", file, file.length());
    }

    private Map<String, Object> readDataFromJson() throws IOException {
        try (InputStream resourceStream = getClass().getClassLoader().getResourceAsStream(this.schemaResourcePath)) {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(resourceStream, new TypeReference<Map<String, Object>>() {
            });
        }
    }

    public static void main(String[] args) throws IOException, TemplateException {
        PropFeedMessageGenerator generator = new PropFeedMessageGenerator(
                "com/prooftrading/core/data/propfeed/iex/TOPS.json",
                "src/main/resources/com/prooftrading/core/data/propfeed/iex/PropFeedMessage.ftl",
                "src/main/resources/com/prooftrading/core/data/propfeed/iex/PropFeedMessageFactory.ftl");

        generator.generate(
                "com.prooftrading.core.data.propfeed.iex.tops",
                new File("src/main/java/com/prooftrading/core/data/propfeed/iex/tops"),
                new ArrayList<>(),
                new ArrayList<>());
        LOG.info("Done");
    }
}
