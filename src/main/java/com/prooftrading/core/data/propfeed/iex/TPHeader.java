package com.prooftrading.core.data.propfeed.iex;

public interface TPHeader {
    byte getVersion(); 
    short getProtocol();
    int getChannelId();
    int getSessionId(); 
    short getPayloadLen();
    short getMessageCount();
    long getStreamOffset();
    long getFirstSequence();
    long getSendTime();
}
