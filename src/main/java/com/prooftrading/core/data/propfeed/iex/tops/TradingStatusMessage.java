package com.prooftrading.core.data.propfeed.iex.tops;

import com.prooftrading.core.data.propfeed.iex.PropFeedMessage;
import com.prooftrading.core.data.propfeed.iex.TPPacket;

/**
 * Generated Message Class
 */
public class TradingStatusMessage extends PropFeedMessage.AbstractPropFeedMessage {

    public static final char MESSAGE_TYPE = 'H';

    public static final char TRADINGSTATUS_HALTED = 'H';
    public static final char TRADINGSTATUS_ORDERACCEPTANCE = 'O';
    public static final char TRADINGSTATUS_PAUSEDORDERACCEPTANCE = 'P';
    public static final char TRADINGSTATUS_TRADING = 'T';

    public static final String REASON_HALT_NEWS_PENDING = "T1";
    public static final String REASON_IPO_NOT_YET_TRADING = "IPO1";
    public static final String REASON_IPO_DEFERRED = "IPOD";
    public static final String REASON_MARKET_WIDE_CIRCUIT_BREAKER_LEVEL_3 = "MCB3";
    public static final String REASON_NOT_AVAILABLE = "NA";
    public static final String REASON_HALT_NEWS_DISSEMINATION = "T2";
    public static final String REASON_IPO_ORDER_ACCEPTANCE_PERIOD = "IPO2";
    public static final String REASON_IPO_PRE_LAUNCH_PERIOD = "IPO3";
    public static final String REASON_MARKET_WIDE_CIRCUIT_BREAKER_LEVEL_1 = "MCB1";
    public static final String REASON_MARKET_WIDE_CIRCUIT_BREAKER_LEVEL_2 = "MCB2";

    private char tradingStatus = 0;
    private long timestamp = 0L;
    private StringBuilder symbol = new StringBuilder();
    private StringBuilder reason = new StringBuilder();

    @Override
    public PropFeedMessage init(TPPacket.TPMessage msg) {
        super.init(msg);
        this.tradingStatus = msg.getChar(1);
        this.timestamp = msg.getLong(2);
        this.symbol = msg.getString(10, 8, this.symbol);
        this.reason = msg.getString(18, 4, this.reason);
        return this;
    }

    public char getTradingStatus() {
        return this.tradingStatus;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public StringBuilder getSymbol() {
        return this.symbol;
    }

    public StringBuilder getReason() {
        return this.reason;
    }

    @Override
    public String toString() {
        return "TradingStatusMessage{" +
                "tradingStatus=" + tradingStatus +
                ", timestamp=" + timestamp +
                ", symbol='" + symbol + "'" +
                ", reason='" + reason + "'" +
                '}';
    }
}